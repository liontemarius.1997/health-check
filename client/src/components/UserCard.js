import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { Icon } from '.';

const UserCard = props => {
  const {
    _id,
    onDelete,
    firstName,
    lastName,
    avatar,
    deleteButton,
    role
  } = props;
  return (
    <UserCardStyle>
      <UserDetailsStyle>
        <Icon s src={avatar} />
        <UserDatesStyle>
          <p>{`${firstName} ${lastName}`}</p>
          <span>{role}</span>
        </UserDatesStyle>
      </UserDetailsStyle>
      {deleteButton && (
        <DeleteButtonWrapper>
          <DeleteButtonStyle>
            <FontAwesomeIcon
              onClick={() => onDelete(_id)}
              icon={faTimes}
              className="icon"
            />
          </DeleteButtonStyle>
        </DeleteButtonWrapper>
      )}
    </UserCardStyle>
  );
};

const UserCardStyle = styled.div`
  width: 100%;
  height: fit-content;
  display: grid;
  grid-template-columns: 1fr 60px;
  border: 0.5px solid #d8d8d8;
  border-radius: 5px;
`;

const UserDetailsStyle = styled.div`
  padding: 12px 16px;
  display: grid;
  align-items: center;
  grid-template-columns: auto 1fr;
  grid-column-gap: 16px;
`;

const UserDatesStyle = styled.div`
  display: grid;
  grid-row-gap: 4px;

  p {
    margin: 0;
    font-size: 14px;
    font-weight: 500;
    line-height: 1.07;
    color: #191919;
  }
  span {
    font-size: 12px;
    color: #434343;
    line-height: 1.25;
  }
`;

const DeleteButtonStyle = styled.p`
  margin: 0;
  height: 15px;
  width: 15px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${({ theme }) => theme.color.error};
  border-radius: 50%;
  cursor: pointer;
  &:hover {
    background-color: #bd4340;
  }
  .icon {
    font-size: 10.5px;
    color: #ffff;
  }
`;

const DeleteButtonWrapper = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
  border-left: 0.5px solid #d8d8d8;
`;

export { UserCard };
