import React from 'react';
import { Query } from 'react-apollo';

import { TopicCard, Checkbox, InputStyle, Spinner, CardsContainer } from '.';
import { TOPICS_QUERY } from '../pages/Topics/query';

const TeamTopics = ({ label, form, field, gridArea }) => {
  const { setFieldValue, errors, touched } = form;
  const { value, name } = field;
  const handleCheckTopic = topicId => {
    const values = [...value] || [];
    const topicIndex = values.findIndex(value => value === topicId);

    if (topicIndex !== -1) {
      values.splice(topicIndex, 1);
    } else {
      values.push(topicId);
    }
    setFieldValue(name, values);
  };
  return (
    <InputStyle style={{ marginTop: '20px' }} gridArea={gridArea}>
      <label>{label}</label>
      <CardsContainer padding="40px 0" columnsNum="4">
        <Query fetchPolicy="network-only" query={TOPICS_QUERY}>
          {({ data, loading, error }) => {
            if (loading) return <Spinner />;
            if (!loading && !error) {
              const { topics } = data;
              return topics.map((topic, index) => (
                <TopicCard
                  checked={value.includes(topic._id)}
                  checkbox
                  {...topic}
                  key={index}
                >
                  <Checkbox
                    onChange={() => handleCheckTopic(topic._id)}
                    checked={value.includes(topic._id)}
                    {...topic}
                  />
                </TopicCard>
              ));
            }
          }}
        </Query>
      </CardsContainer>
      <p className="error">
        {errors[name] && touched[name] && `${errors[name]}`}
      </p>
    </InputStyle>
  );
};

export { TeamTopics };
