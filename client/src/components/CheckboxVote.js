import React from 'react';
import styled from 'styled-components';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const CheckboxVote = ({ color, userId, onChange, values, value: val }) => (
  <FormGroupStyle color={color}>
    <input
      type="radio"
      name={userId}
      id={`${userId}-${val}`}
      value={val}
      onChange={onChange}
      checked={values.some(({ value, id }) => value === val && id === userId)}
    />
    <label htmlFor={`${userId}-${val}`}>
      <span>
        <FontAwesomeIcon icon={faCheck} className="icon" />
      </span>
    </label>
  </FormGroupStyle>
);

export { CheckboxVote };

const FormGroupStyle = styled.div`
  z-index: 100;
  input[type='radio'] {
    display: none;
    &:checked ~ label {
      span {
        opacity: 1;
      }
    }
  }
  label {
    display: inline-block;
    height: 42px;
    width: 42px;
    border-radius: 50%;
    box-shadow: inset 0 2.5px 2.5px -1.5px rgba(0, 0, 0, 0.15);
    background-color: #f0f0f0;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 10000;
  }
  span {
    height: 32px;
    width: 32px;
    border-radius: 50%;
    background-color: #fff;
    display: inline-block;
    opacity: 0;
    background-color: ${({ color }) => color};
    display: flex;
    justify-content: center;
    align-items: center;
    box-shadow: 0 1px 0 0 #919191;
    .icon {
      font-size: 20px;
      color: #fff;
    }
  }
`;
