import React from 'react';
import styled from 'styled-components';

import { Icon } from '.';

const HomeStatsCard = props => {
  return (
    <CardStyle>
      <Icon
        m
        src="https://images.pexels.com/photos/67636/rose-blue-flower-rose-blooms-67636.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
      />
      <CardContentStyle>
        <h1>positive topic</h1>
        <p>dsdas ffsdfsd fdsfs dgfdh ffdfs</p>
      </CardContentStyle>
      <TopicsContainer />
    </CardStyle>
  );
};

const CardStyle = styled.div`
  box-sizing: border-box;
  position: relative;
  width: 230px;
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 16px;
  padding: 24px;
  z-index: 1;
  border: 0.5px solid '#d8d8d8';
  background-color: #ffff;
  border-radius: 5px;
  &:before {
    content: '';
    position: absolute;
    width: 100%;
    height: 1px;
    transform: scaleY(0.5);
    top: 54px;
    left: 0;
    z-index: -1;
    background-color: #d8d8d8;
  }
`;

const CardContentStyle = styled.div`
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 8px;
  max-width: 60px;
  justify-content: center;
  align-items: center;
  align-self: flex-start;
  h1 {
    margin: 0;
    font-size: 12px;
    font-weight: 900;
    line-height: 1.33;
    text-align: center;
    color: #191919;
    text-transform: capitalize;
  }
  p {
    margin: 0;
    font-size: 14px;
    line-height: 1.29;
    color: #191919;
  }
`;

const TopicsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(5, 1fr);
  justify-items: center;
`;

export { HomeStatsCard };
