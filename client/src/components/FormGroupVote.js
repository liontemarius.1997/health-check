import React from 'react';
import styled from 'styled-components';

import { CheckboxVote } from '.';

const FormGroupVote = ({
  def,
  userName,
  userId,
  topicId,
  form,
  autosaveSession = undefined,
  teamId,
  membersLength
}) => {
  const { values, setFieldValue } = form;
  const topicVotes = values[`${topicId}VOTES`] || [];
  const onChange = e => {
    const target = e.currentTarget;
    const targetValue = target.value;
    const targetId = target.name;
    if (target.checked) {
      const voteExist = topicVotes.find(vote => vote.id === targetId);
      if (voteExist) {
        voteExist.value = targetValue;
      } else {
        topicVotes.push({ id: targetId, value: targetValue });
      }
    }
    const userVotes = topicVotes.filter(
      ({ id }) => id !== 'overall-rating' && id !== 'trend'
    );
    if (membersLength !== undefined && userVotes.length === membersLength) {
      const overallRatingVote = Math.round(
        userVotes.reduce((acc, el) => acc + parseInt(el.value), 0) /
          membersLength
      );
      const voteExist = topicVotes.find(vote => vote.id === 'overall-rating');
      if (voteExist) {
        voteExist.value = `${overallRatingVote}`;
      } else {
        topicVotes.push({
          id: 'overall-rating',
          value: `${overallRatingVote}`
        });
      }
    }
    const updatedValues = { ...values, [`${topicId}VOTES`]: topicVotes };
    if (autosaveSession) {
      autosaveSession({
        variables: {
          teamId,
          sessionContent: JSON.stringify(updatedValues)
        }
      });
    }
    setFieldValue(`${topicId}VOTES`, topicVotes);
  };
  return (
    <FormGroupStyle def={def}>
      <p className="userName">{userName}</p>
      <CombinedShapeStyle>
        <CheckboxVote
          color="#e74c3c"
          userId={userId}
          value="0"
          onChange={onChange}
          values={topicVotes}
          topicId={topicId}
        />
        <CheckboxVote
          color="#ffd203"
          userId={userId}
          value="1"
          onChange={onChange}
          values={topicVotes}
          topicId={topicId}
        />
        <CheckboxVote
          color="#55b37e"
          userId={userId}
          value="2"
          onChange={onChange}
          values={topicVotes}
          topicId={topicId}
        />
      </CombinedShapeStyle>
    </FormGroupStyle>
  );
};
export { FormGroupVote };

const FormGroupStyle = styled.div`
  display: flex;
  flex-direction: column;
  .userName {
    font-size: ${({ def }) => (def ? '14px' : '16px')};
    font-weight: ${({ def }) => (def ? '900' : '500')};
    text-transform: ${({ def }) => (def ? 'uppercase' : '')};
    color: #191919;
    margin: 0;
    margin-bottom: 10px;
  }
  &:not(:last-child) {
    margin-bottom: 24px;
  }
`;

const CombinedShapeStyle = styled.div`
  display: grid;
  grid-auto-flow: column;
  justify-content: space-between;
  align-items: center;
  width: 366px;
  position: relative;
  &:before {
    content: '';
    position: absolute;
    left: 0;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
    height: 10px;
    box-shadow: inset 0 1px 2px 0.2px rgba(0, 0, 0, 0.15);
    background-color: #f0f0f0;
    border-radius: 5px;
  }
`;
