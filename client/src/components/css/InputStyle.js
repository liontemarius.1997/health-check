import styled, { css } from 'styled-components';

const InputStyle = styled.div`
  display: grid;
  max-width: ${({ width }) => (width !== undefined ? width : '100%')};
  grid-auto-flow: rows;
  grid-row-gap: 4px;
  grid-area: ${({ gridArea }) => gridArea};
  label {
    font-size: 13.5px;
    font-weight: 700;
    color: #191919;
    text-transform: capitalize;
  }
  p.error {
    height: 16px;
    display: inline-block;
    color: #e74c3c;
    font-size: 12px;
    margin: 0;
  }
  input[type='text'],
  input[type='password'],
  input[type='search'],
  .selectInput {
    border: 1px solid #d8d8d8;
    background-color: #ffff;
    padding: 11px 8px;
    font-weight: 500;
    color: #919191;
    font-size: 13px;
    font-family: inherit;
    outline: none;
    &:focus {
      border-color: #55b37e;
    }
    ${({ serverError }) =>
      serverError &&
      css`
        color: #cb5654;
        border-color: #cb5654;
        background: #ffe8e7;
      `}
  }
`;

export default InputStyle;
