import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
html {
    box-sizing: border-box;
}
*,*:before,*:after{
    box-sizing: inherit;
    padding:0;
    margin:0;
}
body{
    padding:0;
    margin:0;
    font-family: 'Lato', sans-serif;
}
button, a, input {
    font-family: 'Lato', sans-serif;
    outline: none;
    text-decoration: none;
}
`;

export default GlobalStyle;
