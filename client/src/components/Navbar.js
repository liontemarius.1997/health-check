import React, { useEffect, useState, useRef } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import {
  faHome,
  faChartPie,
  faUsers,
  faClipboard,
  faCaretDown
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Icon, NavbarLink } from '.';

const Navbar = props => {
  const { user, signout } = props;
  const { firstName, avatar, lastName } = user;
  const [isOpen, setIsOpen] = useState(false);
  const ref = useRef(null);

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true);
    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    };
  });
  const handleClickOutside = event => {
    if (!ref.current.contains(event.target)) {
      setIsOpen(false);
    }
    if (ref.current.contains(event.target)) {
      setIsOpen(!isOpen);
    }
  };
  return (
    <NavbarStyle>
      <ContentStyle>
        <LinkSection>
          <NavbarLink name="home" to="/" exact icon={faHome} />
          <NavbarLink name="teams" to="/teams" icon={faUsers} />
          <NavbarLink name="stats" to="/stats" icon={faChartPie} />
          <NavbarLink name="topics" to="/topics" icon={faClipboard} />
        </LinkSection>
        <UserLinkStyle>
          <Icon xs src={avatar} />

          <UserNameLink to="/user">{`${firstName} ${lastName}`}</UserNameLink>
          <IconWrapper ref={ref}>
            <FontAwesomeIcon icon={faCaretDown} className="icon" />
          </IconWrapper>
          <DropdownMenuStyle open={isOpen}>
            <LinkStyle to="/user"> user profile</LinkStyle>

            <LinkStyle to="#" onClick={() => signout()}>
              logout
            </LinkStyle>
          </DropdownMenuStyle>
        </UserLinkStyle>
      </ContentStyle>
    </NavbarStyle>
  );
};

const NavbarStyle = styled.div`
  width: 100%;
  position: relative;
  background-color: #094452;
  height: 84px;
`;

const ContentStyle = styled.div`
  max-width: 1000px;
  margin: 0 auto;
  background-color: transparent;
  height: 100%;
  display: flex;
  justify-content: space-between;
`;

const LinkSection = styled.div`
  display: grid;
  grid-auto-flow: column;
  grid-column-gap: 15px;
`;

const UserLinkStyle = styled.div`
  position: relative;
  height: 100%;
  display: grid;
  grid-auto-flow: column;
  grid-column-gap: 15px;
  justify-content: center;
  align-items: center;
`;

const UserNameLink = styled(Link)`
  text-transform: capitalize;
  cursor: pointer;
  font-size: 14px;
  color: #f5f5f7;
  &:hover {
    color: #ece6ce;
  }
`;

const DropdownMenuStyle = styled.div`
  background-color: #fff;
  display: ${({ open }) => (open ? 'flex' : 'none')};
  flex-direction: column;
  position: absolute;
  top: 70%;
  left: 0;
  right: 0;
  width: 100%;
  overflow: hidden;
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
  border: 1px solid ${({ open }) => (open ? '#D8D8D8' : 'transparent')};
`;

const LinkStyle = styled(Link)`
  width: 100%;
  padding: 10px 8px;
  font-size: 14px;
  color: #000;
  cursor: pointer;
  text-transform: capitalize;
  &:hover {
    background-color: #f6f6f6;
    color: #000;
  }
`;

const IconWrapper = styled.div`
  .icon {
    font-size: 15px;
    color: #ffff;
    cursor: pointer;
    &:hover {
      color: #ece6ce;
    }
  }
`;

export { Navbar };
