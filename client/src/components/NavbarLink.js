import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';

const NavbarLink = props => {
  const { icon, name } = props;
  return (
    <ItemStyle {...props}>
      <FontAwesomeIcon className="icon" icon={icon} />
      <h1>{name}</h1>
    </ItemStyle>
  );
};

const ItemStyle = styled(NavLink)`
  text-decoration: none;
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 2.5px;
  align-content: center;
  justify-content: center;
  color: #ffff;
  font-weight: 900;
  letter-spacing: 0.5px;
  width: 60px;
  cursor: pointer;
  position: relative;
  &:hover,
  &.active {
    color: #ece6ce;
  }
  &:before {
    position: absolute;
    content: '';
    height: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: #55b37e;
    border-top-left-radius: 2.5px;
    border-top-right-radius: 2.5px;
    transition: all 0.3s;
  }
  h1 {
    margin: 0;
    padding: 0;
    font-size: 10px;
    text-transform: uppercase;
  }
  .icon {
    justify-self: center;
    font-size: 20px;
  }
  &:active,
  &.active,
  &:hover {
    &:before {
      height: 8px;
    }
  }
`;

export { NavbarLink };
