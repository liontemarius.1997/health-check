import React from 'react';
import styled from 'styled-components';

import { Positive, Negative, TopicSession } from '.';

const CriteriaCard = ({
  positive,
  description,
  results,
  type,
  paddingBotton
}) => {
  return positive ? (
    <CardStyle paddingBotton={paddingBotton}>
      <InfoStyle>LAST HEALTH CHECKS</InfoStyle>
      <Positive dimension="62" />
      <DescriptionStyle>
        <h1>positive {type}</h1>
        <p>{description}</p>
      </DescriptionStyle>
      {results && (
        <StatsSummaryStyle>
          {results.map((result, index) => (
            <TopicSession {...result} key={index} />
          ))}
        </StatsSummaryStyle>
      )}
    </CardStyle>
  ) : (
    <CardStyle paddingBotton={paddingBotton}>
      <InfoStyle>last health check</InfoStyle>
      <Negative dimension="62" />
      <DescriptionStyle negative>
        <h1>negative {type}</h1>
        <p>{description}</p>
      </DescriptionStyle>
      {results && (
        <StatsSummaryStyle>
          {results.map((result, index) => (
            <TopicSession {...result} key={index} />
          ))}
        </StatsSummaryStyle>
      )}
    </CardStyle>
  );
};
export { CriteriaCard };

const CardStyle = styled.div`
  width: 100%;
  overflow: hidden;
  padding: 24px;
  padding-bottom: ${({ paddingBotton }) => paddingBotton};
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 14px;
  position: relative;
  justify-items: start;
  z-index: 1;
  border-radius: 5px;
  background-color: #fff;
  border: solid 0.5px ${({ theme }) => theme.color.borderColor};
  &:before {
    content: '';
    position: absolute;
    top: 54px;
    left: 0;
    width: 100%;
    height: 1px;
    transform: scaleY(0.5);
    background-color: ${({ theme }) => theme.color.borderColor};
    z-index: -1;
  }
`;

const DescriptionStyle = styled.div`
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 5px;
  h1 {
    margin: 0;
    color: ${({ theme, negative }) =>
      negative ? theme.color.error : theme.color.green};
    line-height: 1.67;
    text-transform: uppercase;
    font-weight: 900;
    font-size: 12px;
  }
  p {
    font-size: 14px;
    line-height: 1.29;
    color: ${({ theme }) => theme.color.primary};
    text-align: left;
    margin: 0;
  }
`;

const StatsSummaryStyle = styled.div`
  margin: 0 auto;
  display: grid;
  grid-auto-flow: column;
  grid-column-gap: 15px;
  justify-items: center;
`;

const InfoStyle = styled.p`
  position: absolute;
  top: 16px;
  right: 24px;
  font-size: 10px;
  font-weight: 500;
  line-height: 1.2;
  text-align: right;
  color: #7e7e7e;
  text-transform: uppercase;
`;
