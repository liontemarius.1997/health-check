import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';

const Style = css`
  width: ${({ auto }) => (auto ? 'fit-content' : '100%')};
  border: ${({ def, prim, sec }) => {
    if (def || prim) return '2px solid #55b37e';
    if (sec) return '2px solid #434343';
  }};
  background-color: ${({ def, prim, sec }) => {
    if (def) return '#55b37e';
    if (prim || sec) return '#ffff';
  }};
  color: ${({ def, prim, sec }) => {
    if (def) return '#ffff';
    if (prim) return '#55b37e';
    if (sec) return '#434343';
  }};
  padding: 10px 18px;
  font-size: 13px;
  font-weight: 900;
  text-align: center;
  text-transform: uppercase;
  border-radius: 4px;
  cursor: pointer;
  transition: all 0.1s;
  grid-area: ${({ gridArea }) => gridArea};
  justify-self: center;
  align-self: center;
  &:hover,
  &:focus {
    background-color: ${({ def }) => {
      if (def) return `#3A9461`;
    }};
    border: ${({ def, prim }) => {
      if (def || prim) return `2px solid #3A9461`;
    }};
    color: ${({ prim }) => {
      if (prim) return '#3A9461';
    }};
  }
  &:disabled {
    background-color: #f0f0f0;
    border: 2px solid #d8d8d8;
    color: #919191;
    cursor: not-allowed;
  }
`;

const Button = styled.button`
  ${Style}
`;
const RedirectLink = styled(Link)`
  ${Style}
`;

export { Button, RedirectLink };
