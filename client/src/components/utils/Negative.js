import React from 'react';
import styled from 'styled-components';

const Svg = styled.svg`
  display: block;
  margin: ${props => (props.auto ? 'auto' : '0')};
`;

const Negative = ({ dimension, auto }) => {
  return (
    <Svg
      auto={auto}
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width={dimension}
      height={dimension}
      viewBox="0 0 62 62"
    >
      <defs>
        <circle id="a" cx="31" cy="31" r="31" />
      </defs>
      <g fill="none" fillRule="evenodd">
        <mask id="b" fill="#fff">
          <use xlinkHref="#a" />
        </mask>
        <use fill="#EEE" stroke="#D8D8D8" strokeWidth=".5" xlinkHref="#a" />
        <circle cx="31" cy="31" r="32" fill="#ECE6CE" mask="url(#b)" />
        <path
          fill="#F0918F"
          d="M16.896 33.98h-3.905a.99.99 0 1 1 0-1.98h3.905a.99.99 0 1 1 0 1.98zM50.896 33.98H46.99a.99.99 0 1 1 0-1.98h3.905a.99.99 0 1 1 0 1.98z"
          mask="url(#b)"
        />
        <path
          fill="#191919"
          d="M17.99 28.78a.99.99 0 0 1-.99-.99v-3.563a.99.99 0 1 1 1.981 0v3.563a.99.99 0 0 1-.99.99zM45.99 28.78a.99.99 0 0 1-.99-.99v-3.563a.99.99 0 0 1 1.981 0v3.563a.99.99 0 0 1-.99.99z"
          mask="url(#b)"
        />
        <path
          fill="#D6CEAF"
          d="M37.911 58.277c-17.697 0-32.044-14.323-32.044-31.991 0-9.44 4.096-17.923 10.61-23.78C6.101 7.803-1 18.578-1 31.009-1 48.677 13.347 63 31.044 63a31.954 31.954 0 0 0 21.435-8.213 31.952 31.952 0 0 1-14.568 3.49z"
          mask="url(#b)"
        />
        <path
          fill="#191919"
          d="M39.192 44.096c-.448 0-.882-.248-1.116-.687-1.195-2.252-3.462-3.65-5.916-3.65-2.39 0-4.683 1.412-5.983 3.687-.362.633-1.145.84-1.75.46-.603-.38-.8-1.203-.437-1.836 1.758-3.077 4.889-4.988 8.17-4.988 3.379 0 6.5 1.926 8.144 5.025.343.645.122 1.46-.493 1.82-.197.115-.41.169-.62.169z"
          mask="url(#b)"
        />
      </g>
    </Svg>
  );
};

export { Negative };
