import React from 'react';

const Wrong = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="245"
      height="100"
      viewBox="0 0 245 100"
    >
      <g fill="none" fillRule="evenodd" transform="translate(-3 -13)">
        <circle
          cx="125.903"
          cy="62.903"
          r="49.903"
          fill="#ECE6CE"
          fillRule="nonzero"
        />
        <path
          fill="#D6CEAF"
          fillRule="nonzero"
          d="M136.596 105.438c-27.56 0-49.902-22.342-49.902-49.902 0-14.725 6.378-27.958 16.522-37.092C87.06 26.704 76 43.512 76 62.903c0 27.56 22.342 49.902 49.903 49.902 12.836 0 24.538-4.848 33.38-12.81a49.692 49.692 0 0 1-22.687 5.443z"
        />
        <g transform="translate(99.92 64.182)">
          <circle
            cx="26.406"
            cy="16.886"
            r="6.778"
            fill="#191919"
            fillRule="nonzero"
          />
          <path
            fill="#F0918F"
            d="M9.081 4.217h-6.3a1.597 1.597 0 1 1 0-3.194h6.3a1.598 1.598 0 1 1 0 3.194zM51.648 4.217h-6.3a1.597 1.597 0 1 1 0-3.194h6.3a1.597 1.597 0 1 1 0 3.194z"
          />
        </g>
        <path
          fill="#191919"
          d="M108.243 59.04a1.597 1.597 0 0 1-1.598-1.598v-5.748a1.598 1.598 0 1 1 3.196 0v5.748c0 .882-.716 1.597-1.598 1.597zM145.34 59.04a1.598 1.598 0 0 1-1.598-1.598v-5.748a1.598 1.598 0 0 1 3.195 0v5.748c0 .882-.714 1.597-1.597 1.597z"
        />
        <text
          fill="#191919"
          fontFamily="Avenir-Heavy, Avenir"
          fontSize="100"
          fontWeight="600"
        >
          <tspan x=".4" y="100">
            4
          </tspan>
        </text>
        <text
          fill="#191919"
          fontFamily="Avenir-Heavy, Avenir"
          fontSize="100"
          fontWeight="600"
        >
          <tspan x="192.4" y="100">
            4
          </tspan>
        </text>
      </g>
    </svg>
  );
};

export { Wrong };
