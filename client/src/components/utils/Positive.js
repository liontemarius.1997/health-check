import React from 'react';
import styled from 'styled-components';

const Svg = styled.svg`
  display: block;
  margin: ${props => (props.auto ? 'auto' : '0')};
`;

const Positive = ({ dimension, auto }) => {
  return (
    <Svg
      auto={auto}
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width={dimension}
      height={dimension}
      viewBox="0 0 64 64"
    >
      <defs>
        <circle id="a" cx="31" cy="31" r="31" />
      </defs>
      <g fill="none" fillRule="evenodd" transform="translate(1 1)">
        <circle
          cx="31"
          cy="31"
          r="31"
          fill="#EEE"
          stroke="#D8D8D8"
          strokeWidth=".5"
        />
        <mask id="b" fill="#fff">
          <use xlinkHref="#a" />
        </mask>
        <use fill="#EEE" stroke="#D8D8D8" strokeWidth=".5" xlinkHref="#a" />
        <path
          fill="#ECE6CE"
          d="M62.92 31.284C62.92 48.8 48.72 63 31.204 63 13.687 63-.512 48.8-.512 31.284c0-17.516 14.2-31.716 31.716-31.716 17.516 0 31.716 14.2 31.716 31.716z"
          mask="url(#b)"
        />
        <path
          fill="#D6CEAF"
          d="M4.77 31.284C4.77 14.658 17.566 1.021 33.846-.32a32.01 32.01 0 0 0-2.641-.111c-17.517 0-31.716 14.2-31.716 31.716C-.512 48.8 13.688 63 31.204 63c.89 0 1.77-.039 2.641-.11-16.28-1.343-29.074-14.98-29.074-31.606z"
          mask="url(#b)"
        />
        <path
          fill="#191919"
          d="M16.874 29.437a.99.99 0 0 1-.99-.99v-3.563a.99.99 0 1 1 1.98 0v3.563a.99.99 0 0 1-.99.99z"
          mask="url(#b)"
        />
        <path
          fill="#F0918F"
          d="M16.46 34.348h-3.905a.99.99 0 1 1 0-1.981h3.906a.99.99 0 1 1 0 1.98z"
          mask="url(#b)"
        />
        <path
          fill="#191919"
          d="M45.533 29.437a.99.99 0 0 1-.99-.99v-3.563a.99.99 0 0 1 1.981 0v3.563a.99.99 0 0 1-.99.99z"
          mask="url(#b)"
        />
        <path
          fill="#F0918F"
          d="M49.852 34.348h-3.905a.99.99 0 1 1 0-1.981h3.905a.99.99 0 1 1 0 1.98z"
          mask="url(#b)"
        />
        <path
          fill="#191919"
          d="M38.22 44.15a10.633 10.633 0 0 0 3.653-8.037 2.24 2.24 0 0 0-.659-1.59 2.241 2.241 0 0 0-1.591-.66h-1.972l-6.447 2.073-6.448-2.073h-1.972a2.25 2.25 0 0 0-2.25 2.25 10.632 10.632 0 0 0 3.653 8.037H38.22z"
          mask="url(#b)"
        />
        <path
          fill="#F0918F"
          d="M38.22 44.15a10.614 10.614 0 0 1-7.016 2.633c-2.688 0-5.143-.992-7.017-2.633 1.37-1.664 4-2.79 7.017-2.79 3.017 0 5.646 1.126 7.016 2.79z"
          mask="url(#b)"
        />
        <path
          fill="#F5F5F7"
          d="M37.651 33.863V35.6a2.41 2.41 0 0 1-2.409 2.409h-8.077a2.41 2.41 0 0 1-2.41-2.409v-1.738h12.896z"
          mask="url(#b)"
        />
      </g>
    </Svg>
  );
};

export { Positive };
