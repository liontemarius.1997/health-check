import React from 'react';
import styled from 'styled-components';

const Back = props => {
  return (
    <Svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="24"
      viewBox="0 0 24 24"
    >
      <g fill="none" fillRule="evenodd">
        <path d="M0 0h24v24H0z" />
        <path
          stroke="#434343"
          strokeLinecap="round"
          strokeWidth="2"
          d="M14.5 7.5l-4.533 4.272a1 1 0 0 0 0 1.456L14.5 17.5"
        />
      </g>
    </Svg>
  );
};

const Svg = styled.svg`
  cursor: pointer;
  margin-bottom: -2px;
`;

export { Back };
