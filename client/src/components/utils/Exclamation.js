import React from 'react';

const Exclamation = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="72"
      height="72"
      viewBox="0 0 62 62"
    >
      <g fill="none" fillRule="evenodd">
        <path d="M0 0h62v62H0z" />
        <path
          fill="#CB5654"
          d="M28.928 37.568L27.885 12.04a1 1 0 0 1 1-1.041h5.052a1 1 0 0 1 1 1.04l-1.043 25.528a1 1 0 0 1-.999.959h-2.968a1 1 0 0 1-1-.96zM31.27 51C28.91 51 27 49.226 27 47.022c0-2.205 1.91-4.033 4.27-4.033 2.304 0 4.215 1.828 4.215 4.033 0 2.204-1.91 3.978-4.214 3.978z"
        />
      </g>
    </svg>
  );
};
export { Exclamation };
