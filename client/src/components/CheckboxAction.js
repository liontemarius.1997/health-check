import React from 'react';
import styled from 'styled-components';

import { Checkbox } from '.';

const CheckboxAction = props => {
  const { auto } = props;
  return (
    <ActionPointStyle auto={auto}>
      <Checkbox {...props} />
      <p>{props.text}</p>
    </ActionPointStyle>
  );
};

const CheckboxActions = props => (
  <TopicActionsStyle>
    {props.topicActions.map((topic, index) => (
      <CheckboxAction key={index} {...topic} onChange={props.onChange} />
    ))}
  </TopicActionsStyle>
);

const TopicActionsStyle = styled.div`
  display: grid;
  grid-template-columns: repeat(3, auto);
  grid-column-gap: 24px;
  grid-row-gap: 14px;
  align-items: flex-start;
`;

const ActionPointStyle = styled.div`
  display: grid;
  grid-template-columns: 16px 1fr;
  grid-column-gap: 8px;
  align-items: flex-start;
  p {
    margin: 0;
    max-width: ${({ auto }) => (auto ? '100%' : '207px')};
    height: fit-content;
    font-size: 14px;
    line-height: 1.3;
    color: ${({ theme }) => theme.color.primary};
  }
`;

export { CheckboxAction, CheckboxActions };
