import React, { useState } from 'react';
import styled from 'styled-components';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Field } from 'formik';

import {
  Negative,
  Positive,
  CheckboxAction,
  Icon,
  FormGroupVote,
  Textarea,
  Button
} from '.';

const ActionPointsList = ({ field }) => {
  const { value } = field;
  if (Boolean(value.includes('* '))) {
    return value
      .split('* ')
      .map(
        (action, index) =>
          action !== '' && (
            <CheckboxAction
              hover="true"
              auto="true"
              key={index}
              text={action}
            />
          )
      );
  } else {
    return '';
  }
};

const TopicVote = ({ topic, members, formProps, autosaveSession, teamId }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [preview, setPreview] = useState(false);
  const { positive_criteria, negative_criteria, name, _id, largeIcon } = topic;
  return (
    <ContentStyle>
      <TopicCriteriaStyle>
        <Negative />
        <p>{negative_criteria}</p>
      </TopicCriteriaStyle>
      <TopicVotesStyle>
        <TopicSumarryStyle>
          <Icon xl src={largeIcon} />
          <h1>{name}</h1>
        </TopicSumarryStyle>
        <VotesContainerStyle>
          {members.map((member, index) => (
            <FormGroupVote
              key={index}
              form={formProps}
              userName={`${member.firstName} ${member.lastName}`}
              topicId={_id}
              userId={member._id}
              autosaveSession={autosaveSession}
              teamId={teamId}
              membersLength={members.length}
            />
          ))}
          <FormGroupVote
            def
            userName="overall rating"
            userId="overall-rating"
            topicId={_id}
            form={formProps}
            autosaveSession={autosaveSession}
            teamId={teamId}
          />
          <FormGroupVote
            def
            userName="trend"
            userId="trend"
            topicId={_id}
            form={formProps}
            autosaveSession={autosaveSession}
            teamId={teamId}
          />
        </VotesContainerStyle>
        <ActionsContainerStyle>
          <ToggleButtonStyle onClick={() => setIsOpen(!isOpen)}>
            <FontAwesomeIcon
              className="icon"
              icon={faAngleDown}
              rotation={isOpen ? null : 180}
            />
          </ToggleButtonStyle>
          {isOpen && (
            <React.Fragment>
              {!preview ? (
                <ActionInputStyle>
                  <Field
                    name={`${_id}`}
                    render={props => (
                      <Textarea
                        autosaveSession={autosaveSession}
                        teamId={teamId}
                        label="topic actions"
                        {...props}
                        id={_id}
                        placeholder="* Type your action point here..."
                      />
                    )}
                  />
                  <Button sec auto onClick={() => setPreview(!preview)}>
                    preview
                  </Button>
                </ActionInputStyle>
              ) : (
                <ActionPointsStyle>
                  <h1>action points</h1>
                  <ActionPointsListStyle>
                    <Field
                      name={`${_id}`}
                      render={props => <ActionPointsList {...props} />}
                    />
                  </ActionPointsListStyle>
                  <Button sec auto onClick={() => setPreview(!preview)}>
                    edit
                  </Button>
                </ActionPointsStyle>
              )}
            </React.Fragment>
          )}
        </ActionsContainerStyle>
      </TopicVotesStyle>
      <TopicCriteriaStyle>
        <Positive />
        <p>{positive_criteria}</p>
      </TopicCriteriaStyle>
    </ContentStyle>
  );
};
export { TopicVote };

const ContentStyle = styled.div`
  height: 100%;
  width: 100%;
  display: grid;
  grid-template-columns: minmax(min-content, 230px) 366px minmax(
      min-content,
      230px
    );
  justify-content: space-evenly;
  align-items: center;
`;

const TopicCriteriaStyle = styled.div`
  display: grid;
  justify-content: center;
  grid-row-gap: 24px;
  p {
    margin: 0;
    font-size: 16px;
    line-height: 1.5;
    color: #434343;
    text-align: center;
    font-style: oblique;
  }
  svg {
    height: 82px;
    width: 82px;
    margin: 0 auto;
  }
`;

const TopicVotesStyle = styled.div`
  height: 100%;
  display: grid;
  grid-template-rows: auto 1fr;
  justify-content: center;
  padding: 64px 0;
  position: relative;
`;

const TopicSumarryStyle = styled.div`
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 8px;
  justify-content: center;
  width: 100%;
  border-bottom: 1px solid #f0f0f0;
  padding-bottom: 31.5px;
  h1 {
    font-size: 20px;
    font-weight: 900;
    color: #191919;
    text-align: center;
    display: block;
    margin: 0;
    text-transform: capitalize;
  }
`;

const VotesContainerStyle = styled.div`
  width: fit-content;
  max-height: 45vh;
  display: flex;
  flex-direction: column;
  overflow-y: auto;
  &::-webkit-scrollbar-track {
    background-color: #ffff;
  }

  &::-webkit-scrollbar {
    width: 5px;
    background-color: #ffff;
  }

  &::-webkit-scrollbar-thumb {
    background-color: #ddd;
    height: 60px;
  }
`;

const ActionsContainerStyle = styled.div`
  position: absolute;
  bottom: 0;
  width: 103%;
  max-height: fit-content;
  background-color: transparent;
  display: grid;
  grid-auto-flow: row;
  z-index: 1000;
  transform: translateX(-5px);
`;

const ToggleButtonStyle = styled.div`
  width: 80px;
  height: 40px;
  border: 0.5px solid #d8d8d8;
  background-color: #ffff;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  border-bottom: none;
  display: flex;
  justify-self: center;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  .icon {
    font-size: 22px;
    display: inline-block;
  }
`;

const ActionInputStyle = styled.div`
  width: 100%;
  height: fit-content;
  padding: 16px 5px;
  display: grid;
  grid-auto-flow: row;
  width: 100%;
  background-color: #ffff;
  border-top: 1px solid #d8d8d8;
  textarea {
    height: 96px;
  }
  button {
    justify-self: end;
  }
`;

const ActionPointsStyle = styled.div`
  background-color: #ffff;
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 8px;
  width: 100%;
  padding: 16px 0px;
  padding: 12px 16px;
  h1 {
    margin: 0;
    text-transform: capitalize;
    font-size: 14px;
    font-weight: 900;
    color: #191919;
  }
  button {
    justify-self: end;
  }
`;

const ActionPointsListStyle = styled.div`
  width: 100%;
  height: 100px;
  overflow-y: auto;
  padding: 12px 8px;
  & > * {
    margin-bottom: 10px;
  }
  &::-webkit-scrollbar-track {
    background-color: #ffff;
  }

  &::-webkit-scrollbar {
    width: 5px;
    background-color: #ffff;
  }

  &::-webkit-scrollbar-thumb {
    background-color: #ddd;
    height: 60px;
  }
`;
