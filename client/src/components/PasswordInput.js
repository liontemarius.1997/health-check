import React, { useRef, useState } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye } from '@fortawesome/free-solid-svg-icons';

import { InputStyle, ServerError } from './index';

const PasswordInput = props => {
  const { serverError } = props;
  const serverErrorExist =
    serverError !== undefined && Object.entries(serverError).length > 0;
  const input = useRef(null);
  const [type, setType] = useState('password');
  const { form, field, id, label } = props;
  const { errors, touched } = form;
  const clickHandler = () => {
    input.current.focus();
    type === 'text' ? setType('password') : setType('text');
  };
  return (
    <InputStyle serverError={serverErrorExist}>
      <label htmlFor={id}>{label}</label>
      <FieldStyle>
        <input ref={input} {...field} id={id} placeholder={label} type={type} />
        <FontAwesomeIcon onClick={clickHandler} icon={faEye} className="icon" />
      </FieldStyle>
      {serverError && !errors[id] ? (
        <ServerError {...props} />
      ) : (
        <p className="error">{errors[id] && touched[id] && `${errors[id]}`}</p>
      )}
    </InputStyle>
  );
};

const FieldStyle = styled.div`
  display: grid;
  grid-template-columns: 1fr 40px;
  grid-template-areas: 'input icon';
  align-items: center;
  input {
    grid-column: 1 / end;
    grid-row: 1;
    &:focus ~ .icon {
      color: #55b37e;
    }
  }
  .icon {
    color: #d8d8d8;
    grid-area: icon;
    z-index: 10;
  }
`;

export { PasswordInput };
