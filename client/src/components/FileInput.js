import React, { useRef, useState } from 'react';
import styled, { css } from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { InputStyle, Icon } from '.';

const FileInput = ({
  field,
  form,
  label,
  id,
  disabled,
  setDisabled,
  register
}) => {
  const file = useRef(null);
  const [path, setPath] = useState('');
  const { onBlur, value, name } = field;
  const { setFieldValue, errors, touched } = form;
  const handleUploadIcon = async e => {
    if (file.current === null) return;
    const currentFile = file.current.files[0];
    const data = new FormData();
    data.append('file', currentFile);
    data.append('upload_preset', 'health-check');
    setDisabled(true);
    const res = await fetch(
      'https://api.cloudinary.com/v1_1/dow92xzsk/image/upload',
      {
        method: 'POST',
        body: data
      }
    );
    const result = await res.json();
    setFieldValue(name, result.secure_url);
    setFieldValue('largeIcon', result.eager[0].secure_url);
    setDisabled(false);
    setPath(currentFile.name);
  };

  const handleDelete = () => {
    setPath('');
    file.current.value = '';
    setFieldValue(name, '');
    setFieldValue('largeIcon', '');
  };
  return (
    <InputStyle>
      <label htmlFor={id}>{label}</label>
      <FormGroupStyle register={register}>
        <input
          ref={file}
          onBlur={onBlur}
          type="file"
          id={id}
          name={id}
          onChange={!disabled ? handleUploadIcon : null}
        />
        <Label disabled={disabled} htmlFor={id}>
          {value !== '' && value !== undefined ? (
            <p>uploaded</p>
          ) : (
            <p>upload{disabled ? 'ing' : ''}</p>
          )}
        </Label>
        {path !== '' && (
          <React.Fragment>
            <Icon s src={value} />
            {register ? (
              <p>{path.slice(0, 12) + '...'}</p>
            ) : (
              <p>{path.length > 25 ? `${path.slice(0, 25)}...` : path}</p>
            )}
            <DeleteButtonStyle onClick={handleDelete}>
              <FontAwesomeIcon className="icon" icon={faTimes} />
            </DeleteButtonStyle>
          </React.Fragment>
        )}
      </FormGroupStyle>
      <p className="error">{errors[id] && touched[id] && `${errors[id]}`}</p>
    </InputStyle>
  );
};

const FormGroupStyle = styled.div`
  display: flex;
  align-items: center;
  grid-auto-flow: column;
  & > * {
    margin-right: ${({ register }) => (register ? '5px' : '16px')};
  }
  input[type='file'] {
    display: none;
  }
  p {
    font-size: 15px;
    font-weight: 500;
    color: #434343;
  }
`;

const Label = styled.label`
  cursor: pointer;
  width: auto;
  height: 40px;
  padding: 0 40px;
  border: 2px solid ${({ theme }) => theme.color.green};
  background-color: #ffff;
  border-radius: 4px;
  display: flex;
  justify-content: center;
  align-items: center;
  &:hover {
    border: 2px solid #3a9461;
    p {
      color: #3a9461;
    }
  }
  p {
    color: ${({ theme }) => theme.color.green};
    margin: 0;
    font-size: 13px;
    font-weight: 900;
    text-align: center;
    font-family: inherit;
    text-transform: uppercase;
  }
  ${({ disabled }) =>
    disabled &&
    css`
      background-color: #f0f0f0;
      border: 2px solid #d8d8d8;
      p {
        color: #919191;
      }
      &:hover {
        border: 2px solid #d8d8d8;
        p {
          color: #919191;
        }
      }
      cursor: not-allowed;
    `}
`;

const DeleteButtonStyle = styled.p`
  margin: 0;
  height: 15px;
  width: 15px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${({ theme }) => theme.color.error};
  border-radius: 50%;
  cursor: pointer;
  &:hover {
    background-color: #bd4340;
  }
  .icon {
    font-size: 10.5px;
    color: #ffff;
  }
`;
export { FileInput };
