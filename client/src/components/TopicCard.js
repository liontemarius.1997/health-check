import React from 'react';
import styled, { css } from 'styled-components';

import { Positive, Negative, Icon } from '.';

const TopicCard = props => {
  const {
    children,
    positive_criteria,
    negative_criteria,
    icon,
    name,
    checkbox,
    topicActions,
    info
  } = props;
  return (
    <CardStyle {...props}>
      {info && <InfoStyle>last health check</InfoStyle>}
      {checkbox && <CheckboxStyle>{children}</CheckboxStyle>}
      <Icon l src={icon} />
      <TitleStyle>
        {name.length > 25 ? `${name.slice(0, 22)}...` : name}
      </TitleStyle>
      <ContentStyle>
        {children && topicActions ? (
          children
        ) : (
          <React.Fragment>
            <CriteriaStyle>
              <Positive dimension="24" />
              <p>
                {positive_criteria.length > 80
                  ? `${positive_criteria.slice(0, 80)}...`
                  : positive_criteria}
              </p>
            </CriteriaStyle>
            <CriteriaStyle>
              <Negative dimension="24" />
              <p>
                {negative_criteria.length > 80
                  ? `${negative_criteria.slice(0, 80)}...`
                  : negative_criteria}
              </p>
            </CriteriaStyle>
          </React.Fragment>
        )}
      </ContentStyle>
    </CardStyle>
  );
};

const CardStyle = styled.div`
  position: relative;
  width: 100%;
  display: grid;
  grid-template-columns: repeat(3, fit-content);
  grid-auto-rows: max-content;
  grid-row-gap: 16px;
  padding: 24px;
  z-index: 1;
  border: 0.5px solid
    ${({ theme, checked }) =>
      checked ? theme.color.green : theme.color.borderColor};
  background-color: #ffff;
  border-radius: 5px;
  cursor: ${({ pointer }) => pointer && 'pointer'};
  transition: all 0.2s;
  &:before {
    content: '';
    position: absolute;
    width: 100%;
    height: 1px;
    top: 54px;
    left: 0px;
    z-index: -1;
    transform: scaleY(0.5);
    background-color: ${({ theme }) => theme.color.borderColor};
  }
  ${({ hover }) =>
    hover ||
    css`
      &:hover {
        transform: translateY(-3px);
        box-shadow: 0px 5px 12px -3px rgba(0, 0, 0, 0.25);
      }
    `}
`;

const CheckboxStyle = styled.div`
  width: fit-content;
  height: fit-content;
  position: absolute;
  top: 16px;
  right: 16px;
`;

const TitleStyle = styled.p`
  margin: 0;
  text-transform: uppercase;
  color: ${({ theme }) => theme.color.primary};
  font-weight: 900;
  font-size: 12px;
`;

const ContentStyle = styled.div`
  display: grid;
  grid-template-rows: repeat(2, auto);
  grid-row-gap: 16px;
`;

const CriteriaStyle = styled.div`
  display: grid;
  grid-template-columns: 24px 1fr;
  grid-column-gap: 8px;
  align-items: flex-start;
  p {
    word-wrap: break-word;
    margin: 0;
    max-width: 150px;
    line-height: 1.33;
    color: ${({ theme }) => theme.color.primary};
    font-size: 12px;
  }
`;

const InfoStyle = styled.p`
  position: absolute;
  top: 16px;
  right: 24px;
  font-size: 10px;
  font-weight: 500;
  line-height: 1.2;
  text-align: right;
  color: #7e7e7e;
  text-transform: uppercase;
`;

export { TopicCard };
