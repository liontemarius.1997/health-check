import React from 'react';
import styled from 'styled-components';

const Icon = props => {
  return <IconStyle {...props} />;
};

const IconStyle = styled.img`
  overflow: none;
  background: #ffff;
  display: inline-block;
  border-radius: 50%;
  object-fit: fill;
  border: solid 0.75px #d8d8d8;
  width: ${({ xl, l, m, s, xs }) => {
    if (xl) return '128px';
    if (l) return '62px';
    if (m) return '48px';
    if (s) return '40px';
    if (xs) return '30px';
  }};
  height: ${({ xl, l, m, s, xs }) => {
    if (xl) return '128px';
    if (l) return '62px';
    if (m) return '48px';
    if (s) return '40px';
    if (xs) return '30px';
  }};
`;

export { Icon };
