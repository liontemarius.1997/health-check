import React from 'react';
import styled from 'styled-components';
import debounce from 'debounce';

import { InputStyle } from '.';

const Textarea = ({
  form,
  field,
  id,
  label,
  height,
  placeholder,
  gridArea,
  autosaveSession = undefined,
  teamId
}) => {
  const { errors, touched, values } = form;
  const { onChange, onBlur, value } = field;
  const handleAutosave = debounce(() => {
    if (autosaveSession === undefined) return;
    autosaveSession({
      variables: {
        teamId,
        sessionContent: JSON.stringify(values)
      }
    });
  }, 800);
  return (
    <InputStyle gridArea={gridArea}>
      <label htmlFor={id}>{label}</label>
      <TextareaStyle
        height={height}
        onChange={e => {
          onChange(e);
          handleAutosave();
        }}
        onBlur={onBlur}
        name={id}
        placeholder={placeholder}
        value={value}
        id={id}
      />
      <p className="error">{errors[id] && touched[id] && `${errors[id]}`}</p>
    </InputStyle>
  );
};

const TextareaStyle = styled.textarea`
  width: 100%;
  height: ${({ height }) => height};
  border: 1px solid #d8d8d8;
  background-color: #ffff;
  padding: 11px 8px;
  font-weight: 500;
  color: #919191;
  font-size: 14px;
  font-family: inherit;
  outline: none;
  &::-webkit-scrollbar-track {
    background-color: #ffff;
  }

  &::-webkit-scrollbar {
    width: 5px;
    background-color: #ffff;
  }

  &::-webkit-scrollbar-thumb {
    background-color: #ddd;
    height: 60px;
  }
`;

export { Textarea };
