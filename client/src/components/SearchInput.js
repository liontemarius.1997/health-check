import React, { useRef, useState, useEffect } from 'react';
import styled from 'styled-components';
import { faTimes, faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { InputStyle, Button, Icon } from '.';

const SearchInput = props => {
  useEffect(() => {
    document.addEventListener('click', handleOutsideClick, true);
    return () => {
      document.removeEventListener('click', handleOutsideClick, true);
    };
  });
  const {
    fetchedUsers,
    setFetchedUsers,
    form,
    field,
    id,
    label,
    onChange,
    gridArea
  } = props;
  const { setFieldValue, values, errors, touched } = form;
  const { value, name, onBlur } = field;
  const [user, setUser] = useState({});
  const [isOpen, setIsOpen] = useState(true);
  const ref = useRef(null);
  const inputGroup = useRef(null);
  const handleOutsideClick = event => {
    if (!inputGroup.current.contains(event.target)) {
      setIsOpen(false);
    }
  };
  const currentUserHandler = user => {
    setUser(user);
    setFetchedUsers([]);
    setFieldValue(name, user.fullName);
  };
  const addUserHandler = () => {
    const members = values['members'] || [];
    const result = members.find(member => member._id === user._id);
    if (result) return;
    members.push(user);
    setFieldValue('members', members);
    clearInputHandler();
  };

  const clearInputHandler = () => {
    setFieldValue(name, '');
    setUser({});
    ref.current.focus();
    setFetchedUsers([]);
  };
  return (
    <InputStyle gridArea={gridArea}>
      <label htmlFor={id}>{label}</label>
      <ContentStyle>
        <SearchStyle ref={inputGroup}>
          <input
            autoComplete="off"
            value={value}
            onBlur={onBlur}
            onChange={e => {
              onChange(e);
              setIsOpen(true);
            }}
            id={id}
            type="text"
            ref={ref}
          />
          {value === '' ? (
            <FontAwesomeIcon
              icon={faSearch}
              onClick={() => ref.current.focus()}
              className="searchIcon"
            />
          ) : (
            <FontAwesomeIcon
              icon={faTimes}
              onClick={clearInputHandler}
              className="clearIcon"
            />
          )}
          {isOpen && fetchedUsers.length > 0 && (
            <ResultsStyle>
              {fetchedUsers.map((user, index) => (
                <ResultStyle
                  key={index}
                  onClick={() => currentUserHandler(user)}
                >
                  <Icon xs src={user.avatar} />
                  <p>{user.fullName}</p>
                </ResultStyle>
              ))}
            </ResultsStyle>
          )}
        </SearchStyle>
        <Button prim type="button" onClick={addUserHandler}>
          add
        </Button>
      </ContentStyle>
      <p className="error">
        {errors['members'] && touched['members'] && `${errors['members']}`}
      </p>
    </InputStyle>
  );
};
export { SearchInput };

const ContentStyle = styled.div`
  display: grid;
  grid-template-columns: 1fr 77px;
  grid-column-gap: 8px;
  grid-area: ${({ gridArea }) => gridArea};
`;

const SearchStyle = styled.div`
  display: grid;
  grid-template-columns: 1fr 40px;
  align-items: center;
  position: relative;
  .searchIcon,
  .clearIcon {
    color: #d8d8d8;
    z-index: 10;
    grid-row: 1;
    grid-column: 2 / end;
    cursor: pointer;
    justify-self: center;
    align-self: center;
  }
  input[type='text'] {
    grid-column: 1 / end;
    grid-row: 1;
    &:focus ~ .searchIcon,
    &:focus ~ .clearIcon {
      color: ${({ theme: { color } }) => color.green};
    }
  }
`;

const ResultsStyle = styled.div`
  position: absolute;
  top: 100%;
  border: 1px solid #d8d8d8;
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
  left: 0;
  right: 0;
  max-height: 200px;
  overflow-y: auto;
  z-index: 10000;
  &::-webkit-scrollbar-track {
    background-color: #f5f5f5;
  }

  &::-webkit-scrollbar {
    width: 5px;
    background-color: #f5f5f5;
  }

  &::-webkit-scrollbar-thumb {
    background: #919191;
  }
`;

const ResultStyle = styled.div`
  cursor: pointer;
  width: 100%;
  height: 50px;
  display: flex;
  align-items: center;
  background-color: #fff;
  & > * {
    margin-right: 10px;
  }
  &:hover {
    background-color: #f6f6f6;
  }
  img {
    height: 40px;
    width: 40px;
    border-radius: 50%;
    object-fit: content;
  }
  p {
    margin: 0;
    font-weight: 500;
    font-size: 14px;
    &:hover {
      color: ${({ theme: { color } }) => color.primary};
    }
  }
`;
