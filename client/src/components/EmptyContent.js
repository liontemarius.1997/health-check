import React from 'react';
import styled from 'styled-components';

import { Sleep } from '.';

const EmptyContent = ({ firstRow, secondRow, children }) => {
  return (
    <ContentStyle>
      <Sleep />
      <span>
        <h1>{firstRow}</h1>
        <h1>{secondRow}</h1>
      </span>
      {children}
    </ContentStyle>
  );
};
export { EmptyContent };

const ContentStyle = styled.div`
  padding-top: 10px;
  width: 100%;
  height: fit-content;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  & > * {
    margin-bottom: 15px;
  }
  h1 {
    font-size: 20px;
    font-weight: 900;
    line-height: 1.5;
    text-align: center;
    color: #191919;
  }
`;
