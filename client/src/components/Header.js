import React from 'react';
import styled from 'styled-components';

const Header = ({ children }) => {
  return <SimpleHeaderStyle>{children}</SimpleHeaderStyle>;
};

const ExtendedHeader = ({ children }) => {
  return <ComplexHeaderStyle>{children}</ComplexHeaderStyle>;
};

const SimpleHeaderStyle = styled.div`
  width: fit-content;
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-column-gap: 8px;
  align-items: center;
  h1 {
    text-transform: capitalize;
    margin: 0;
    color: #191919;
    line-height: 1.2;
    font-size: 20px;
    font-size: 20px;
    font-weight: 400;
  }
  span {
    cursor: pointer;
    height: 24px;
    width: 24px;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 15px;
    color: #191919;
  }
`;

const ComplexHeaderStyle = styled.div`
  width: fit-content;
  display: grid;
  grid-auto-flow: column;
  grid-column-gap: 31px;
  align-items: center;
  div {
    width: fit-content;
    height: 30px;
    display: grid;
    align-items: center;
    padding-right: 24px;
    border-right: 1px solid #cfcfcf;
    grid-auto-flow: column;
    grid-column-gap: 10px;
    h1 {
      text-transform: capitalize;
      margin: 0;
      color: ${({ theme }) => theme.color.primary};
      line-height: 1.2;
      font-size: 20px;
      font-weight: 400;
      font-size: ${({ fontSize }) => `${fontSize}px`};
    }
    span {
      cursor: pointer;
      height: 24px;
      width: 24px;
      display: flex;
      justify-content: center;
      align-items: center;
      font-size: 15px;
      color: #191919;
    }
  }
`;

export { Header, ExtendedHeader };
