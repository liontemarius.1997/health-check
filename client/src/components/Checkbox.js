import React from 'react';
import styled, { css } from 'styled-components';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Checkbox = props => {
  return (
    <CheckboxStyle hover={props.hover}>
      <input type="checkbox" id={props._id} name={props._id} {...props} />
      <label htmlFor={props._id}>
        <FontAwesomeIcon icon={faCheck} className="icon" />
      </label>
    </CheckboxStyle>
  );
};

const CheckboxStyle = styled.div`
  display: flex;
  input[type='checkbox'] {
    display: none;
    &:checked ~ label {
      background-color: ${({ theme }) => theme.color.green};
      .icon {
        opacity: 1;
      }
    }
  }
  label {
    ${({ hover }) =>
      hover ||
      css`
        cursor: pointer;
      `};
    height: 16px;
    width: 16px;
    border-radius: 3px;
    border: 1px solid #d8d8d8;
    display: flex;
    justify-content: center;
    align-items: center;
    .icon {
      color: #ffff;
      font-size: 11px;
      opacity: 0;
    }
  }
`;

export { Checkbox };
