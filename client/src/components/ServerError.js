import React from 'react';
import styled from 'styled-components';

import { WarningTriangle } from '.';

const ServerError = props => {
  const { serverError: error, errorIdentifier } = props;
  if (!error || !errorIdentifier) return null;
  if (
    error.networkError &&
    error.networkError.result &&
    error.newtorkError.result.errors.length
  ) {
    return error.newtworkError.result.errors.map((error, i) => (
      <ErrorStyle key={i}>
        <WarningTriangle />
        <p className="error" data-test="graphql-error">
          {error.message.replace('GraphQL error: ', '')}
        </p>
      </ErrorStyle>
    ));
  }
  return (
    <ErrorStyle>
      <WarningTriangle />
      <p className="error" data-test="graphql-error">
        {error.message.replace('GraphQL error: ', '').includes(errorIdentifier)
          ? error.message.replace('GraphQL error: ', '')
          : null}
      </p>
    </ErrorStyle>
  );
};

const ErrorStyle = styled.div`
  display: flex;
  align-items: center;
  svg {
    font-size: 5px;
  }
`;

export { ServerError };
