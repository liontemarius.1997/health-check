import React from 'react';
import styled from 'styled-components';

import { Wrong, ServerError } from '.';

const NotFound = ({ children, error }) => {
  return (
    <ContainerStyle>
      <Wrong />
      <ServerError errorIdentifier=" " error={error} />
      {children}
    </ContainerStyle>
  );
};

const ContainerStyle = styled.div`
  width: 100%;
  height: fit-content;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  p {
    font-size: 20px;
  }
  & > * {
    margin-bottom: 20px;
  }
`;

export { NotFound };
