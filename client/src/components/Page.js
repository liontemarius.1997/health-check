import React from 'react';
import styled, { ThemeProvider } from 'styled-components';

import { GlobalStyle } from './index';

const theme = {
  color: {
    background: '#f6f6f6',
    primary: '#191919',
    error: '#CB5654',
    green: '#55B37E',
    menuBar: '#094452',
    secondary: '#434343',
    submenu: '#ECE6CE',
    buttonText: '#7e7e7e',
    borderColor: '#d8d8d8',
    yellow: '#ffd203'
  },
  maxWidth: '1000px'
};

const Page = ({ children }) => {
  return (
    <ThemeProvider theme={theme}>
      <React.Fragment>
        <GlobalStyle />
        <PageStyle>{children}</PageStyle>
      </React.Fragment>
    </ThemeProvider>
  );
};

const PageStyle = styled.div`
  min-height: 100vh;
  max-width: 100vw;
  color: ${({ theme }) => theme.color.primary};
  background-color: ${({ theme }) => theme.color.background};
`;

export { Page };
