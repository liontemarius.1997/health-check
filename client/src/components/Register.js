import React from 'react';
import styled, { keyframes } from 'styled-components';

const Register = ({ children, disabled, register }) => {
  return (
    <Fieldset register={register} disabled={disabled} aria-busy={disabled}>
      <RegisterStyle>{children}</RegisterStyle>
    </Fieldset>
  );
};

const loading = keyframes`
from {
  border:1px solid #2ecc71;
  opacity: 1;
}
to {
  border: 1px solid #27ae60;
  opacity: 0.4;
}
`;

const Fieldset = styled.fieldset`
  position: relative;
  transform: ${({ register }) =>
    register ? 'translateY(50px)' : 'translateY(100px)'};
  width: 486px;
  margin: 0 auto;
  height: fit-content;
  border: none;
  &:disabled,
  &[aria-busy='true'] {
    animation: ${loading} 1s linear infinite;
  }
`;

const RegisterStyle = styled.div`
  width: 100%;
  height: fit-content;
  padding: 42px 85px 47px 85px;
  background-color: #ffff;
  position: relative;
  border-radius: 8px;
  h1 {
    margin: 0;
    max-width: 188px;
    margin: 0 auto;
    color: #191919;
    line-height: 1.23;
    text-align: center;
    font-size: 26px;
    font-weight: 900;
    margin-bottom: 24px;
    text-transform: capitalize;
  }
`;

export { Register };
