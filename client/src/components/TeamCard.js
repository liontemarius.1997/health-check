import React from 'react';
import styled from 'styled-components';
import dateFormat from 'dateformat';

import { Icon } from '.';

const TeamCard = ({
  name,
  membersNumber,
  description,
  data,
  icon,
  onClick
}) => {
  const createdAt = data.length ? data[0].createdAt : false;
  return (
    <CardStyle onClick={onClick}>
      <InfoStyle>
        {createdAt
          ? `last health check\n ${dateFormat(createdAt, 'dd mmmm yyyy')}`
          : `no health check done yet`}
      </InfoStyle>
      <Icon l src={icon} />
      <TitleStyle>
        <h1>{name}</h1>
        <p>
          ({membersNumber} member{membersNumber > 1 ? 's' : ''})
        </p>
      </TitleStyle>
      <DescriptionStyle>{description}</DescriptionStyle>
    </CardStyle>
  );
};

const CardStyle = styled.div`
  color: ${({ theme }) => theme.color.primary};
  max-width: 230px;
  grid-auto-rows: max-content;
  position: relative;
  padding: 24px;
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 16px;
  background-color: #ffff;
  border-radius: 5px;
  border: 0.5px solid #d8d8d8;
  cursor: pointer;
  transition: all 0.2s;
  backface-visibility: hidden;
  z-index: 1;
  &:before {
    z-index: -1;
    position: absolute;
    content: '';
    width: 100%;
    height: 1px;
    transform: scaleY(0.5);
    background-color: #d8d8d8;
    left: 0;
    top: 54px;
  }
  &:hover {
    transform: translateY(-3px);
    box-shadow: 0px 5px 12px -3px rgba(0, 0, 0, 0.25);
  }
`;

const TitleStyle = styled.div`
  display: grid;
  grid-auto-flow: row;

  h1 {
    text-transform: capitalize;
    font-size: 20px;
    font-weight: 900;
    margin: 0;
    word-wrap: break-word;
    color: inherit;
  }
  p {
    font-size: 12px;
    margin: 0;
    word-wrap: break-word;
    color: inherit;
  }
`;

const DescriptionStyle = styled.p`
  margin-top: -2px;
  font-size: 14px;
  color: inherit;
  margin: 0;
  max-width: 183px;
`;

const InfoStyle = styled.p`
  position: absolute;
  color: inherit;
  top: 16px;
  right: 24px;
  font-size: 10px;
  font-weight: 500;
  color: ${({ theme }) => theme.color.buttonText};
  text-align: right;
  line-height: 1.2;
  text-transform: uppercase;
  max-width: 110px;
  margin: 0;
`;

export { TeamCard };
