import React, { useState } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';

import { InputStyle } from '.';

const SelectInput = ({ form, field }) => {
  const { name, value } = field;
  const { setFieldValue } = form;
  const [active, setActive] = useState(false);
  return (
    <InputStyle>
      <label onClick={() => setActive(true)}>Role</label>
      <InputContainerStyle>
        <SelectInputStyle active={active}>
          <p>{value}</p>
        </SelectInputStyle>
        <IconStyle onClick={() => setActive(!active)}>
          <FontAwesomeIcon icon={faAngleDown} className="icon" />
        </IconStyle>
        <InputOptionsStyle active={active}>
          <OptionsStyle>
            <p
              onClick={e => {
                setFieldValue(name, 'Designer');
                setActive(false);
              }}
            >
              Designer
            </p>
            <p
              onClick={e => {
                setFieldValue(name, 'Developer');
                setActive(false);
              }}
            >
              Developer
            </p>
            <p
              onClick={e => {
                setFieldValue(name, 'Product Owner');
                setActive(false);
              }}
            >
              Product Owner
            </p>
            <p
              onClick={e => {
                setFieldValue(name, 'Team Lead');
                setActive(false);
              }}
            >
              Team Lead
            </p>
          </OptionsStyle>
        </InputOptionsStyle>
      </InputContainerStyle>
      <p className="error" />
    </InputStyle>
  );
};
export { SelectInput };
const InputContainerStyle = styled.div`
  display: grid;
  grid-template-columns: 1fr 40px;
  position: relative;
`;

const IconStyle = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  grid-column: 2/3;
  cursor: pointer;
  grid-row: 1;
  .icon {
    grid-column: 2/3;
    grid-row: 1;
    z-index: 100;
    font-size: 17px;
    margin: auto;
    color: #434343;
  }
`;

const InputOptionsStyle = styled.div`
  z-index: 1000;
  position: absolute;
  top: 100%;
  left: 0;
  background-color: ${({ active }) => (active ? '#ffff' : 'transparent')};
  right: 0;
  height: ${({ active }) => (!active ? '0px' : 'fit-content')};
  overflow: hidden;
  border: solid 1px ${({ active }) => (active ? '#d8d8d8' : 'transparent')};
`;

const OptionsStyle = styled.div`
  display: flex;
  flex-direction: column;
  p {
    &:not(:last-child) {
      border-bottom: 1px solid #d8d8d8;
    }
    margin: 0;
    font-size: 12px;
    text-transform: capitalize;
    background-color: #fff;
    padding: 11px 8px;
    &:hover {
      background-color: #f6f6f6;
    }
  }
`;

const SelectInputStyle = styled.div`
  grid-row: 1;
  grid-column: 1 / end;
  border: solid 1px ${({ active }) => (active ? '#55b37e' : '#d8d8d8')};
  background-color: #ffffff;
  padding: 11px 8px;
  p {
    font-size: 13px;
    font-weight: 500;
    color: #191919;
    text-transform: capitalize;
  }
`;
