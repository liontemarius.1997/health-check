import React from 'react';
import styled from 'styled-components';

import { UserCard } from '.';

const UsersContainer = ({ form, field, gridArea, marginTop }) => {
  const { value, name } = field;
  const { setFieldValue } = form;
  const onDelete = id => {
    const updatedUsers = value.filter(({ _id }) => _id !== id);
    setFieldValue(name, updatedUsers);
  };
  if (value.length) {
    return (
      <FullContainerStyle marginTop={marginTop} gridArea={gridArea}>
        {value.map((user, index) => (
          <UserCard key={index} {...user} onDelete={onDelete} deleteButton />
        ))}
      </FullContainerStyle>
    );
  } else {
    return (
      <EmptyCotainerStyle marginTop={marginTop} gridArea={gridArea}>
        <p>add team members</p>
      </EmptyCotainerStyle>
    );
  }
};

const UsersList = ({ members }) => (
  <FullContainerStyle>
    {members.map((user, index) => (
      <UserCard key={index} {...user} />
    ))}
  </FullContainerStyle>
);

const EmptyCotainerStyle = styled.div`
  grid-area: ${({ gridArea }) => gridArea};
  width: 49%;
  height: fit-content;
  margin-top: ${({ marginTop }) => marginTop};
  padding: 20px 0;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 5px;
  border: solid 1px #d8d8d8;
  background-color: #f0f0f0;
  p {
    margin: 0;
    font-weight: 900;
    font-size: 12px;
    color: #919191;
    text-transform: uppercase;
  }
`;

const FullContainerStyle = styled.div`
  grid-area: ${({ gridArea }) => gridArea};
  width: 100%;
  margin-top: ${({ marginTop }) => marginTop};
  height: auto;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 24px;
  background-color: transparent;
`;

export { UsersList, UsersContainer };
