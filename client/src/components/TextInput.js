import React from 'react';

import { InputStyle, ServerError } from './index';

const TextInput = props => {
  const { form, field, id, label, serverError, displayError } = props;
  const { errors, touched } = form;
  const serverErrorExist =
    serverError !== undefined && Object.entries(serverError).length > 0;
  return (
    <InputStyle serverError={serverErrorExist}>
      <label htmlFor={id}>{label}</label>
      <input
        autoComplete="off"
        type="text"
        placeholder={label}
        {...field}
        id={id}
      />
      {serverError && displayError && !errors[id] ? (
        <ServerError {...props} />
      ) : (
        <p className="error">{errors[id] && touched[id] && `${errors[id]}`}</p>
      )}
    </InputStyle>
  );
};

export { TextInput };
