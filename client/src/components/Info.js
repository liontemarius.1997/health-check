import React from 'react';
import styled from 'styled-components';

import { Medal, Hearth, Exclamation } from '.';

const Info = ({ negative, positive }) => {
  if (negative.length === 0) {
    return (
      <ContentStyle>
        <Medal />
        <p>
          Congratulations! You guys broke the Health Check algorythm. You only
          have positive topics and none negative. Doc is proud of you. Keep it
          up!
        </p>
      </ContentStyle>
    );
  } else if (positive.length === 0) {
    return (
      <ContentStyle>
        <Hearth />
        <p>
          You guys are in the emergency room right now. It looks like you have
          only negative topics and none positive. Get some defribillation and
          get back on your feet!
        </p>
      </ContentStyle>
    );
  } else {
    return (
      <ContentStyle>
        <Exclamation />
        Would you look at that! Your team is improving on their{' '}
        {positive.map(({ name }, index) =>
          index + 2 < positive.length
            ? ` ${name},`
            : index + 1 === positive.length && positive.length > 1
            ? ` and ${name}`
            : ` ${name} `
        )}{' '}
        topic{positive.length > 1 ? 's' : ''}. Just watch out on your{' '}
        {negative.map(({ name }, index) =>
          index + 2 < negative.length
            ? ` ${name},`
            : index + 1 === negative.length && negative.length > 1
            ? ` and ${name}`
            : ` ${name} `
        )}{' '}
        and you'll do even better.
      </ContentStyle>
    );
  }
};
export { Info };

const ContentStyle = styled.div`
  width: 100%;
  height: fit-content;
  padding: 15px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #fff;
  border-radius: 5px;
  p {
    margin: 0;
    margin-left: 10px;
    font-size: 16px;
    line-height: 1.5;
    color: ${({ theme: { color } }) => color.primary};
  }
`;
