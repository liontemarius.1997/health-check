import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';

import { Icon } from '.';

const TopicSession = props => {
  const { icon, trend, overallRating, name } = props;
  return (
    <TopicStyle>
      <TopicIconStyle status={trend}>
        <Icon m src={icon} />
        <span>
          <FontAwesomeIcon
            className="icon"
            icon={faAngleDown}
            rotation={overallRating > 1 ? 180 : overallRating === 1 ? 90 : null}
          />
        </span>
      </TopicIconStyle>
      <h1>{name.length > 20 ? `${name.slice(0, 17)}...` : name} </h1>
    </TopicStyle>
  );
};
export { TopicSession };

const TopicIconStyle = styled.div`
  height: fit-content;
  width: fit-content;
  justify-self: center;
  position: relative;
  span {
    height: 15px;
    width: 15px;
    position: absolute;
    bottom: 0;
    right: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    background-color: ${({ status, theme }) =>
      status === 0
        ? theme.color.error
        : status === 1
        ? theme.color.yellow
        : theme.color.green};
    .icon {
      font-size: 14px;
      height: 100%;
      color: #ffff;
    }
  }
`;

const TopicStyle = styled.div`
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 8px;
  max-width: 60px;
  justify-content: center;
  align-items: center;
  align-self: flex-start;
  h1 {
    margin: 0;
    font-size: 12px;
    font-weight: 900;
    line-height: 1.33;
    text-align: center;
    color: ${({ theme }) => theme.color.primary};
    text-transform: capitalize;
  }
`;
