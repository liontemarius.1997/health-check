import gql from 'graphql-tag';

const TEAMS_QUERY = gql`
  query TEAMS_QUERY {
    teams {
      _id
      name
    }
  }
`;

export default TEAMS_QUERY;
