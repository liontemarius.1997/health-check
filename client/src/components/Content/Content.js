import React, { useState } from 'react';
import styled from 'styled-components';
import { Query } from 'react-apollo';

import { SubNav, EmptyContent, RedirectLink } from '..';
import TEAMS_QUERY from './query';

const Content = props => {
  const [currentTeam, setCurrentTeam] = useState('');
  return (
    <ContentWrapperStyle>
      <Query
        query={TEAMS_QUERY}
        fetchPolicy="network-only"
        onCompleted={data => {
          const { teams } = data;
          if (teams.length) setCurrentTeam(teams[0]._id);
          else return;
        }}
      >
        {({ data, loading, error }) => {
          if (loading) return null;
          if (!loading && !error) {
            return (
              <React.Fragment>
                <SubNav>
                  {data.teams.length
                    ? data.teams.map(({ name, _id }) => (
                        <TeamNameStyle
                          onClick={() => setCurrentTeam(_id)}
                          active={_id === currentTeam}
                          key={_id}
                        >
                          {name}
                        </TeamNameStyle>
                      ))
                    : null}
                </SubNav>
                {data.teams.length === 0 ? (
                  <Inner>
                    <EmptyContent
                      firstRow="There is no team to health check at the moment."
                      secondRow="Help Doc wake up!"
                    >
                      <RedirectLink def="true" auto="true" to="/teams">
                        create a team
                      </RedirectLink>
                    </EmptyContent>
                  </Inner>
                ) : (
                  <Inner {...props}>{props.children(currentTeam)}</Inner>
                )}
              </React.Fragment>
            );
          }
        }}
      </Query>
    </ContentWrapperStyle>
  );
};

const DefaultContent = props => {
  return (
    <ContentWrapperStyle>
      <SubNav />
      <Inner {...props}>{props.children}</Inner>
    </ContentWrapperStyle>
  );
};

const ContentWrapperStyle = styled.div`
  max-width: 100vw;
  height: fit-content;
  display: flex;
  flex-direction: column;
`;

const TeamNameStyle = styled.p`
  cursor: pointer;
  text-transform: capitalize;
  padding-right: 10px;
  border-right: 1px solid ${({ theme }) => theme.color.buttonText};
  font-size: 12px;
  font-weight: 500;
  color: ${({ active, theme }) =>
    active ? theme.color.primary : theme.color.buttonText};
  text-decoration: ${({ active }) => (active ? 'underline' : 'none')};
  &:not(:first-child) {
    margin-left: 10px;
  }
  &:hover {
    color: ${({ theme }) => theme.color.primary};
    text-decoration: underline;
  }
`;

const Inner = styled.div`
  width: ${({ theme }) => theme.maxWidth};
  min-height: fit-content;
  padding: ${({ padding }) => padding};
  background-color: transparent;
  margin: 0 auto;
`;

export { Content, DefaultContent };
