import React, { useRef, useState } from 'react';
import styled from 'styled-components';
import {
  faAngleDoubleLeft,
  faAngleDoubleRight
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const SubNav = ({ children }) => {
  const [position, setPosition] = useState(0);
  const ref = useRef(null);
  let paths;
  let extendedContainerWidth;
  if (ref.current !== null) {
    extendedContainerWidth = ref.current.scrollWidth - ref.current.clientWidth;
    paths = Math.ceil(extendedContainerWidth / 500);
  }
  const scrollR = () => {
    ref.current.scrollLeft += 500;
    setPosition(position + 1);
  };

  const scrollL = () => {
    ref.current.scrollLeft -= 500;
    setPosition(position - 1);
  };
  return (
    <SubNavStyle>
      <SubNavContentStyle>
        {position > 0 && (
          <FontAwesomeIcon
            className="iconLeft"
            icon={faAngleDoubleLeft}
            onClick={() => scrollL()}
          />
        )}
        <TeamList ref={ref}>{children}</TeamList>
        {position < paths && (
          <FontAwesomeIcon
            className="iconRight"
            icon={faAngleDoubleRight}
            onClick={() => scrollR()}
          />
        )}
      </SubNavContentStyle>
    </SubNavStyle>
  );
};

const SubNavStyle = styled.div`
  max-width: 100vw;
  padding: 8px 0;
  background-color: ${({ theme }) => theme.color.submenu};
`;

const SubNavContentStyle = styled.div`
  max-width: ${({ theme }) => theme.maxWidth};
  margin: 0 auto;
  display: grid;
  grid-template-columns: auto 1fr auto;
  .iconLeft,
  .iconRight {
    cursor: pointer;
    color: #919191;
    font-size: 12px;
    align-self: center;
    justify-self: center;
    transition: all 0.2s;
    &:hover {
      color: #191919;
    }
  }
  .iconRight {
    margin-left: 5px;
  }
  .iconLeft {
    margin-right: 5px;
  }
`;

const TeamList = styled.div`
  height: 100%;
  display: flex;
  overflow-x: hidden;
  scroll-behavior: smooth;
`;

export { SubNav };
