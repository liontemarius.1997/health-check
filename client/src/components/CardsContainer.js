import React from 'react';
import styled from 'styled-components';

const CardsContainer = ({ title, children, columnsNum, marginTop }) => {
  return (
    <ContainerWrapper marginTop={marginTop}>
      <TitleStyle>{title}</TitleStyle>
      <CardsStyle columnsNum={columnsNum}>{children}</CardsStyle>
    </ContainerWrapper>
  );
};

const ContainerWrapper = styled.div`
  width: 100%;
  height: fit-content;
  display: flex;
  flex-direction: column;
  margin-top: ${({ marginTop }) => marginTop};
`;

const TitleStyle = styled.p`
  height: auto;
  display: block;
  margin: 0;
  color: ${({ theme }) => theme.color.primary};
  font-size: 14px;
  font-weight: 900;
`;

const CardsStyle = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(${({ columnsNum }) => columnsNum}, 1fr);
  grid-gap: 24px;
`;

export { CardsContainer };
