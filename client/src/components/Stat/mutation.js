import gql from 'graphql-tag';

const UPDATE_SESSION_MUTATION = gql`
  mutation UPDATE_SESSION_MUTATION(
    $teamId: ID!
    $currentSessionIndex: ID!
    $updatedSession: [SessionContentInput]
  ) {
    updateSession(
      teamId: $teamId
      currentSessionIndex: $currentSessionIndex
      updatedSession: $updatedSession
    ) {
      name
    }
  }
`;

export default UPDATE_SESSION_MUTATION;
