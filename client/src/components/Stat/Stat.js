import React, { useState, useEffect } from 'react';
import styled, { css } from 'styled-components';
import dateFormat from 'dateformat';
import { Mutation } from 'react-apollo';

import { TopicSession, CheckboxActions } from '..';
import UPDATE_SESSION_MUTATION from './mutation';

const Stat = props => {
  const {
    topicsSession,
    createdAt,
    number,
    _id: currentSessionIndex,
    teamId
  } = props;
  const [extend, setExtend] = useState(false);
  const [session, setSession] = useState([]);
  useEffect(() => setSession(topicsSession), [topicsSession]);
  const handleUpdateSession = (event, topic, updatedSession) => {
    const { checked, id } = event.currentTarget;
    const currentTopicSession = session.find(
      ({ topicId }) => topicId === topic
    );
    const currentTopicAction = currentTopicSession.topicActions.find(
      topicAction => topicAction._id === id
    );
    if (checked) {
      currentTopicAction.checked = true;
    } else {
      currentTopicAction.checked = false;
    }
    setSession([...session]);
    session.forEach(s => {
      delete s.__typename;
      s.topicActions.forEach(topicAc => delete topicAc.__typename);
    });
    updatedSession({
      variables: {
        teamId,
        updatedSession: session,
        currentSessionIndex
      }
    });
  };
  return (
    <ContainerStyle>
      <HeaderStyle>
        <h1>session {number}</h1>
        <p>{dateFormat(createdAt, 'dd mmmm yyyy')}</p>
      </HeaderStyle>
      {!extend ? (
        <SessionContentStyle display="gridRow">
          {session.map(topic => (
            <TopicSession key={topic._id} {...topic} />
          ))}
        </SessionContentStyle>
      ) : (
        <Mutation mutation={UPDATE_SESSION_MUTATION} fetchPolicy="no-cache">
          {(updateSession, { loading, error }) => (
            <SessionContentStyle disabled={loading && !error} gap="24px">
              {session.map((topic, index) => (
                <SessionContentExtendedStyle key={index}>
                  <TopicSession {...topic} />
                  <CheckboxActions
                    {...topic}
                    onChange={e =>
                      handleUpdateSession(e, topic.topicId, updateSession)
                    }
                  />
                </SessionContentExtendedStyle>
              ))}
            </SessionContentStyle>
          )}
        </Mutation>
      )}
      <ExpandButtonStyle onClick={() => setExtend(!extend)}>
        <p>see {extend ? 'less' : 'more'}</p>
      </ExpandButtonStyle>
    </ContainerStyle>
  );
};

const ContainerStyle = styled.div`
  display: grid;
  grid-template-rows: 54px 1fr 40px;
  height: fit-content;
  width: 100%;
  border-radius: 5px;
  border: 0.5px solid #d8d8d8;
  background-color: #ffff;
`;

//-------------HEADER-----------

const HeaderStyle = styled.div`
  padding: 16px 24px;
  position: relative;
  display: flex;
  border-bottom: solid 0.5px #d8d8d8;
  align-items: center;
  h1 {
    margin: 0;
    font-size: 12px;
    font-weight: 900;
    line-height: 1.67;
    color: ${({ theme }) => theme.color.primary};
    text-transform: uppercase;
  }
  p {
    position: absolute;
    text-transform: uppercase;
    top: 50%;
    transform: translateY(-50%);
    right: 16px;
    font-size: 10px;
    font-weight: 500;
    line-height: 1.2;
    text-align: right;
    color: #7e7e7e;
    margin: 0;
  }
`;

//-------------CONTENT-------------

const SessionContentStyle = styled.div`
  display: grid;
  grid-auto-flow: ${({ flow }) => flow};
  padding: 23px 90px 16px 90px;
  grid-gap: ${({ gap }) => gap};
  justify-items: center;
  flex-wrap: wrap;
  opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};
  ${({ display }) =>
    display === 'gridRow' &&
    css`
      grid-template-columns: repeat(10, 1fr);
    `}
`;

const SessionContentExtendedStyle = styled.div`
  display: grid;
  grid-template-columns: 60px 1fr;
  grid-column-gap: 25px;
  justify-self: start;
`;

//-----------EXPAND BUTTON----------
const ExpandButtonStyle = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  border-top: solid 0.5px #d8d8d8;
  p {
    margin: 0;
    font-size: 14px;
    font-weight: 900;
    text-align: center;
    color: #7e7e7e;
    text-transform: uppercase;
  }
`;

export { Stat };
