import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { compose, graphql } from 'react-apollo';
import debounce from 'debounce';

import { createTeam, searchUser } from '.';

import {
  DefaultContent,
  Header,
  TextInput,
  Textarea,
  Button,
  FileInput,
  Back,
  UsersContainer,
  TeamTopics,
  SearchInput
} from '../../components';

const validationSchema = Yup.object({
  name: Yup.string().required('Team name is required'),
  icon: Yup.string().required('Team icon is required'),
  members: Yup.array().required('At least one member is required'),
  description: Yup.string().required('Team description is required'),
  topics: Yup.array().required('At least one topic is required')
});

const initialValues = {
  name: '',
  icon: '',
  members: '',
  description: '',
  currentUser: '',
  topics: ''
};

const CreateTeam = props => {
  useEffect(() => {
    document.title = 'Create Team';
  }, []);
  const [loading, setLoading] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [fetchedUsers, setFetchedUsers] = useState([]);
  const { createTeam, createTeamResult, searchUser } = props;
  const { error } = createTeamResult;
  const createTeamHandler = values => {
    setLoading(true);
    const { members, topics, name, icon, description } = values;
    const usersIds = members.map(member => member._id);
    const teamInput = {
      members: usersIds,
      name,
      icon,
      description,
      topics
    };
    createTeam({ variables: { teamInput } })
      .then(() => {
        setLoading(false);
        props.history.push('/teams');
      })
      .catch(() => setLoading(false));
  };

  const handleSearchUser = debounce(async e => {
    const name = e.target.value;
    const {
      data: { userByName }
    } = await searchUser({ variables: { name } });
    setFetchedUsers(userByName);
  }, 600);
  return (
    <DefaultContent padding="40px 0">
      <Fieldset disabled={loading && !error} aria-busy={loading && !error}>
        <Header>
          <Back onClick={() => props.history.push('/teams')} />
          <h1>create team</h1>
        </Header>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={values => createTeamHandler(values, createTeam)}
        >
          {props => (
            <FormStyle>
              <Field
                name="name"
                render={props => (
                  <TextInput
                    {...props}
                    id="name"
                    label="Team Name"
                    serverError={error}
                    errorIdentifier="name"
                    displayError={true}
                  />
                )}
              />
              <Field
                name="icon"
                render={props => (
                  <FileInput
                    {...props}
                    disabled={disabled}
                    setDisabled={setDisabled}
                    id="icon"
                    label="Team Icon"
                  />
                )}
              />

              <Field
                name="currentUser"
                render={props => {
                  const {
                    field: { onChange }
                  } = props;
                  return (
                    <SearchInput
                      fetchedUsers={fetchedUsers}
                      setFetchedUsers={setFetchedUsers}
                      {...props}
                      onChange={e => {
                        onChange(e);
                        handleSearchUser(e);
                      }}
                      id="currentUser"
                      label="Add Team Users"
                      gridArea="searchUser"
                    />
                  );
                }}
              />

              <Field
                name="description"
                render={props => (
                  <Textarea
                    gridArea="description"
                    {...props}
                    height="70px"
                    id="description"
                    label="Team Description"
                    placeholder="Add team description here..."
                  />
                )}
              />
              <Field
                name="members"
                render={props => (
                  <UsersContainer
                    marginTop="-6px"
                    gridArea="members"
                    {...props}
                  />
                )}
              />
              <Field
                name="topics"
                render={props => (
                  <TeamTopics
                    {...props}
                    gridArea="topics"
                    label="team topics"
                    id="topics"
                  />
                )}
              />
              <Button
                gridArea="submit"
                auto
                disabled={disabled || loading}
                def
                type="submit"
              >
                creat{loading ? 'ing' : 'e'} team
              </Button>
            </FormStyle>
          )}
        </Formik>
      </Fieldset>
    </DefaultContent>
  );
};

const Fieldset = styled.fieldset`
  border: none;
  width: 100%;
  height: fit-content;
  background-color: transparent;
  &:disabled {
    opacity: 0.5;
  }
  &[aria-busy='true'] {
    opacity: 0.5;
  }
`;

const FormStyle = styled(Form)`
  margin-top: 30px;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: 24px;
  grid-row-gap: 5px;
  grid-template-areas:
    'name icon'
    'description .'
    'searchUser .'
    'members members'
    'topics topics'
    'submit submit';
`;

export default compose(
  graphql(createTeam, {
    options: { fetchPolicy: 'no-cache' },
    name: 'createTeam'
  }),
  graphql(searchUser, {
    options: { fetchPolicy: 'no-cache' },
    name: 'searchUser'
  })
)(CreateTeam);
