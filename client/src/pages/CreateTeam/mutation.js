import gql from 'graphql-tag';

const createTeam = gql`
  mutation($teamInput: TeamInput!) {
    createTeam(teamInput: $teamInput) {
      _id
    }
  }
`;
export { createTeam };
