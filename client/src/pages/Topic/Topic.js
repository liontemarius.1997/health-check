import React, { useEffect } from 'react';
import { graphql } from 'react-apollo';
import { Redirect } from 'react-router-dom';

import { getTopicDetails } from './query';
import {
  DefaultContent,
  ExtendedHeader,
  Icon,
  Button,
  Spinner,
  CriteriaCard,
  CardsContainer,
  Back
} from '../../components';

const Topic = props => {
  useEffect(() => {
    document.title = 'Topic';
  }, []);
  const { topic, loading, error } = props.topic;

  if (loading || error) {
    return (
      <DefaultContent padding="40px 0">
        {loading && <Spinner />}
        {error && <Redirect to="/login" />}
      </DefaultContent>
    );
  }
  if (!loading && !error) {
    const { name, icon, _id, positive_criteria, negative_criteria } = topic;
    return (
      <DefaultContent padding="40px 0">
        <ExtendedHeader>
          <div>
            <Back onClick={() => props.history.push('/topics')} />
            <h1>{name}</h1>
            <Icon xs src={icon} />
          </div>
          {!topic.public && (
            <Button
              prim
              auto
              onClick={() => props.history.push(`/topics/edit-topic/${_id}`)}
            >
              edit
            </Button>
          )}
        </ExtendedHeader>
        <CardsContainer marginTop="40px" columnsNum="2">
          <CriteriaCard
            type="criteria"
            description={positive_criteria}
            positive
            paddingBotton="65px"
          />
          <CriteriaCard
            type="criteria"
            description={negative_criteria}
            paddingBotton="65px"
          />
        </CardsContainer>
      </DefaultContent>
    );
  }
};

export default graphql(getTopicDetails, {
  options: props => {
    return {
      variables: {
        topicId: props.match.params.topicId
      },
      fetchPolicy: 'no-cache'
    };
  },
  name: 'topic'
})(Topic);
