import gql from 'graphql-tag';
import { TOPIC_DETAILS_FRAGMENT } from '../Topics/query';

const getTopicDetails = gql`
  query($topicId: ID!) {
    topic(topicId: $topicId) {
      ...TopicDetails
      public
    }
  }
  ${TOPIC_DETAILS_FRAGMENT}
`;

export { getTopicDetails };
