import gql from 'graphql-tag';

const USER_QUERY = gql`
  query USER_QUERY {
    me {
      firstName
      lastName
      email
      avatar
      role
      googleAccount
    }
  }
`;

const me = gql`
  query {
    me {
      firstName
      lastName
      email
      avatar
      role
      googleAccount
    }
  }
`;

export { USER_QUERY, me };
