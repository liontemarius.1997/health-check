import gql from 'graphql-tag';

const signout = gql`
  mutation {
    signout {
      msg
    }
  }
`;
export { signout };
