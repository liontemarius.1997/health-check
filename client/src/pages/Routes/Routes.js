import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import {
  Home,
  Login,
  Teams,
  Topics,
  Topic,
  CreateTopic,
  Stats,
  CreateTeam,
  EditTopic,
  Team,
  EditTeam,
  User,
  RegisterPage,
  NotFound,
  CreateStat,
  ResetPassword,
  ForgotPassword
} from '..';
import { Navbar } from '../../components';
import { PrivateRoute, PublicRoute } from '../utils';
import { USER_QUERY, signout, me } from '.';

const Routes = props => {
  const { signout, user } = props;
  const { me, loading, error } = user;
  if (loading && !error) {
    return '';
  } else {
    return (
      <Router>
        {me && <Navbar user={me} signout={signout} />}
        <Switch>
          <PrivateRoute path="/topics/create-topic" component={CreateTopic} />
          <PrivateRoute path="/teams/create-team" component={CreateTeam} />

          <PrivateRoute path="/teams/edit-team/:teamId" component={EditTeam} />
          <PrivateRoute
            path="/topics/edit-topic/:topicId"
            component={EditTopic}
          />
          <PublicRoute path="/reset-token/:token" component={ResetPassword} />
          <PublicRoute path="/forgot-password" component={ForgotPassword} />
          <PrivateRoute path="/topics/:topicId" component={Topic} />
          <PrivateRoute path="/teams/:teamId" component={Team} />
          <PrivateRoute path="/teams" component={Teams} />
          <PrivateRoute path="/stats" component={Stats} />

          <PrivateRoute path="/topics" component={Topics} />
          <PrivateRoute path="/create-stat/:teamId" component={CreateStat} />
          <PrivateRoute path="/user" component={User} />
          <PublicRoute path="/login" component={Login} />
          <PublicRoute path="/register" component={RegisterPage} />
          <PrivateRoute exact path="/" component={Home} />
          <PrivateRoute path="*" exact component={NotFound} />
        </Switch>
      </Router>
    );
  }
};

export default compose(
  graphql(me, { options: { fetchPolicy: 'network-only' }, name: 'user' }),
  graphql(signout, {
    options: {
      refetchQueries: [{ query: USER_QUERY }],
      fetchPolicy: 'no-cache'
    },
    name: 'signout'
  })
)(Routes);
