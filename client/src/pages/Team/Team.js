import React, { useEffect } from 'react';
import styled from 'styled-components';
import { Query } from 'react-apollo';
import { Redirect } from 'react-router-dom';

import {
  InputStyle,
  DefaultContent,
  Icon,
  Button,
  Spinner,
  Back,
  ExtendedHeader,
  UsersList,
  TopicCard
} from '../../components';
import { TEAM_QUERY } from '.';

const Team = props => {
  useEffect(() => {
    document.title = 'Team';
  }, []);
  const teamId = props.match.params.teamId;
  return (
    <DefaultContent padding="40px 0">
      <Query query={TEAM_QUERY} fetchPolicy="no-cache" variables={{ teamId }}>
        {({ data, loading, error }) => {
          if (loading) return <Spinner />;
          if (error) return <Redirect to="/login" />;
          if (!loading && !error) {
            const {
              name,
              icon,
              description,
              topics,
              members,
              _id,
              unfinishedSectionVote
            } = data.team;
            return (
              <React.Fragment>
                <ExtendedHeader>
                  <div>
                    <Back onClick={() => props.history.push('/teams')} />
                    <h1>{name}</h1>
                    <Icon xs src={icon} />
                  </div>
                  <Button
                    def
                    auto
                    onClick={() => props.history.push(`/create-stat/${teamId}`)}
                  >
                    {unfinishedSectionVote === ''
                      ? 'new health check'
                      : 'finish last health check'}
                  </Button>

                  <Button
                    prim
                    auto
                    onClick={() =>
                      props.history.push(`/teams/edit-team/${_id}`)
                    }
                  >
                    edit
                  </Button>
                </ExtendedHeader>
                <TeamPageContainer>
                  <InputStyle width="50%">
                    <label>team description</label>
                    <p>{description}</p>
                  </InputStyle>
                  <InputStyle>
                    <label>team members</label>
                    <UsersList members={members} />
                  </InputStyle>
                  <InputStyle>
                    <label>team topics</label>
                    <TeamTopicsContainer>
                      {topics.map((topic, index) => (
                        <TopicCard {...topic} key={index} />
                      ))}
                    </TeamTopicsContainer>
                  </InputStyle>
                </TeamPageContainer>
              </React.Fragment>
            );
          }
        }}
      </Query>
    </DefaultContent>
  );
};

const TeamPageContainer = styled.div`
  display: grid;
  grid-auto-flow: row;
  padding: 30px 0;
  grid-row-gap: 25px;
`;

const TeamTopicsContainer = styled.div`
  display: grid;
  grid-gap: 24px;
  grid-template-columns: repeat(4, 1fr);
`;
export { Team };
