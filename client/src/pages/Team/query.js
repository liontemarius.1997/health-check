import gql from 'graphql-tag';

const TEAM_QUERY = gql`
  query TEAM_QUERY($teamId: ID!) {
    team(teamId: $teamId) {
      _id
      name
      icon
      description
      admin
      unfinishedSectionVote
      members {
        _id
        firstName
        lastName
        avatar
        role
      }
      topics {
        _id
        name
        icon
        largeIcon
        positive_criteria
        negative_criteria
        public
      }
    }
  }
`;
export { TEAM_QUERY };
