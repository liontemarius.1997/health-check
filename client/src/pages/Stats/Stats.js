import React, { useEffect } from 'react';
import { Query } from 'react-apollo';
import styled from 'styled-components';
import { Redirect } from 'react-router-dom';

import {
  Content,
  Header,
  Spinner,
  Stat,
  EmptyContent,
  RedirectLink
} from '../../components';
import SESSIONS_QUERY from './query';

const Stats = () => {
  useEffect(() => {
    document.title = 'Stats';
  }, []);
  return (
    <Content padding="25px 0">
      {teamId => (
        <React.Fragment>
          <Header>
            <h1>stats</h1>
          </Header>
          {teamId === undefined ? (
            <p>there is no teams</p>
          ) : (
            <Query
              fetchPolicy="no-cache"
              query={SESSIONS_QUERY}
              variables={{ teamId }}
            >
              {({ data, loading, error }) => {
                if (loading) return <Spinner />;
                if (error) return <Redirect to="/login" />;
                if (!loading && !error) {
                  const { sessions } = data;
                  if (sessions.length === 0) {
                    return (
                      <EmptyContent
                        firstRow="There is no stats for this team at the moment."
                        secondRow="Help Doc wake up!"
                      >
                        <RedirectLink
                          def="true"
                          auto="true"
                          to={`/teams/${teamId}`}
                        >
                          create a new health check
                        </RedirectLink>
                      </EmptyContent>
                    );
                  }
                  return (
                    <StatsContainer>
                      {sessions.map((session, index) => (
                        <Stat
                          teamId={teamId}
                          key={index}
                          {...session}
                          number={sessions.length - index}
                        />
                      ))}
                    </StatsContainer>
                  );
                }
              }}
            </Query>
          )}
        </React.Fragment>
      )}
    </Content>
  );
};

const StatsContainer = styled.div`
  padding: 30px 0;
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 24px;
`;

export { Stats };
