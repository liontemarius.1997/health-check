import gql from 'graphql-tag';

const SESSIONS_QUERY = gql`
  query SESSIONS_QUERY($teamId: ID!) {
    sessions(teamId: $teamId) {
      _id
      createdAt
      topicsSession {
        trend
        overallRating
        _id
        topicId
        icon
        name
        topicActions {
          text
          checked
          _id
        }
      }
    }
  }
`;

export default SESSIONS_QUERY;
