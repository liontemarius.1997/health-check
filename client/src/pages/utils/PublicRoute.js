import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { Query } from 'react-apollo';

import { USER_QUERY } from '../Routes';

const PublicRoute = ({ component: Component, ...rest }) => (
  <Query query={USER_QUERY}>
    {({ data, loading, error }) => {
      if (loading) return '';
      if (!loading && !error) {
        return (
          <Route
            {...rest}
            render={props =>
              data.me ? (
                <Redirect
                  to={
                    !(rest.location.rest === undefined)
                      ? rest.location.rest.path.split('/:')[0]
                      : '/'
                  }
                  {...props}
                />
              ) : (
                <Component {...props} />
              )
            }
          />
        );
      }
    }}
  </Query>
);

export { PublicRoute };
