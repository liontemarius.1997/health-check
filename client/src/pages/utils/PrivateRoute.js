import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { Query } from 'react-apollo';

import { USER_QUERY } from '../Routes';

const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Query query={USER_QUERY}>
      {({ data, loading, error }) => {
        if (loading) return '';
        if (!loading && !error) {
          return (
            <Route
              {...rest}
              render={props =>
                data.me ? (
                  <Component {...props} />
                ) : (
                  <Redirect to={{ pathname: '/login', rest }} />
                )
              }
            />
          );
        }
      }}
    </Query>
  );
};
export { PrivateRoute };
