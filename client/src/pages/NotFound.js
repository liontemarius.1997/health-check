import React from 'react';
import styled from 'styled-components';
import { DefaultContent, WrongURL } from '../components';

const NotFound = props => {
  return (
    <DefaultContent padding="40px 0">
      <ContentStyle>
        <WrongURL />
        <h1>
          Oops! <br /> Wrong patient room.
        </h1>
      </ContentStyle>
    </DefaultContent>
  );
};
export { NotFound };

const ContentStyle = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  & > * {
    margin-bottom: 15px;
  }
  h1 {
    font-size: 20px;
    font-weight: 900;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    text-align: center;
    color: ${({ theme: { color } }) => color.primary};
  }
`;
