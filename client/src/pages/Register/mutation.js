import gql from 'graphql-tag';

const REGISTER_MUTATION = gql`
  mutation REGISTER_MUTATION($userInput: UserInput!) {
    signup(userInput: $userInput) {
      email
    }
  }
`;

export { REGISTER_MUTATION };
