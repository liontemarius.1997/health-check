import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { Mutation } from 'react-apollo';

import { REGISTER_MUTATION } from '.';
import { USER_QUERY } from '../Routes';

import {
  Register,
  TextInput,
  PasswordInput,
  Button,
  SelectInput,
  FileInput
} from '../../components';

const initialValues = {
  firstName: '',
  lastName: '',
  password: '',
  avatar: '',
  email: '',
  role: 'developer'
};

const validationSchema = Yup.object({
  firstName: Yup.string().required('This field is required'),
  lastName: Yup.string().required('This field is required'),
  password: Yup.string().required('This field is required'),
  avatar: Yup.string().required('This field is required'),
  email: Yup.string()
    .email('Invalid email')
    .required('This field is required')
});

const RegisterPage = props => {
  useEffect(() => {
    document.title = 'Register';
  }, []);
  const [disabled, setDisabled] = useState(false);
  const signupHandler = async (values, signup) => {
    delete values.largeIcon;
    return await signup({ variables: { userInput: values } });
  };
  return (
    <Mutation
      mutation={REGISTER_MUTATION}
      refetchQueries={[{ query: USER_QUERY }]}
    >
      {(signup, { loading, error }) => (
        <Register register disabled={loading}>
          <h1>health check registration</h1>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={values => signupHandler(values, signup)}
          >
            <RegisterStyle>
              <RowStyle>
                <Field
                  name="firstName"
                  render={props => (
                    <TextInput {...props} label="First Name" id="firstName" />
                  )}
                />
                <Field
                  name="lastName"
                  render={props => (
                    <TextInput {...props} label="Last Name" id="lastName" />
                  )}
                />
              </RowStyle>
              <Field
                name="email"
                render={props => (
                  <TextInput
                    {...props}
                    id="email"
                    label="User Email"
                    serverError={error}
                    errorIdentifier="This email is already in use"
                    displayError={true}
                  />
                )}
              />
              <Field
                name="password"
                render={props => (
                  <PasswordInput {...props} id="password" label="Password" />
                )}
              />
              <Field
                name="role"
                render={props => (
                  <SelectInput {...props} id="role" label="Role" />
                )}
              />
              <Field
                name="avatar"
                render={props => (
                  <FileInput
                    register
                    disabled={disabled}
                    setDisabled={setDisabled}
                    {...props}
                    id="avatar"
                    label="User Picture"
                  />
                )}
              />
              <Button type="submit" def disabled={disabled || loading}>
                Register
              </Button>
            </RegisterStyle>
          </Formik>
        </Register>
      )}
    </Mutation>
  );
};
export { RegisterPage };

const RegisterStyle = styled(Form)`
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 6px;
`;

const RowStyle = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: 10px;
`;
