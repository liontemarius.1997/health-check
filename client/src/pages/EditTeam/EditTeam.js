import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { compose, graphql } from 'react-apollo';
import debounce from 'debounce';
import { Redirect } from 'react-router-dom';

import { getTeamDetails, searchUser, editTeam } from '.';

import {
  DefaultContent,
  Header,
  TextInput,
  Textarea,
  Button,
  FileInput,
  Back,
  SearchInput,
  UsersContainer,
  TeamTopics,
  Spinner
} from '../../components';

const validationSchema = Yup.object({
  name: Yup.string().required('Team name is required'),
  members: Yup.array().required('At least one member is required'),
  description: Yup.string().required('Team description is required'),
  topics: Yup.array().required('At least one topic is required')
});

const EditTeam = props => {
  useEffect(() => {
    document.title = 'Edit Team';
  }, []);
  const { editTeam, editTeamResult, searchUser, team } = props;
  const { teamId } = props.match.params;
  const [disabledForm, setDisabledForm] = useState(false);
  const [fetchedUsers, setFetchedUsers] = useState([]);
  const [loadingForm, setLoadingForm] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const editTeamHandler = values => {
    setLoadingForm(true);
    const { members, topics, name, icon, description } = values;
    const usersIds = members.map(member => member._id);
    const teamInput = {
      members: usersIds,
      name,
      icon,
      description,
      topics
    };
    if (icon === '') delete teamInput.icon;
    editTeam({ variables: { teamInput, teamId } })
      .then(() => {
        setLoadingForm(false);
        props.history.push(`/teams/${teamId}`);
      })
      .catch(() => setLoadingForm(false));
  };

  const handleChanges = ({ values, initialValues }) => {
    setDisabledForm(JSON.stringify(values) === JSON.stringify(initialValues));
  };

  const { team: currentTeam, loading, error } = team;
  if (loading || error) {
    return (
      <DefaultContent padding="40px 0">
        {loading && <Spinner />}
        {error && <Redirect to="/login" />}
      </DefaultContent>
    );
  }
  if (!loading && !error) {
    const { description, name, topics, members } = currentTeam;
    const topicIds = topics.map(({ _id }) => _id);
    const initialValues = {
      name,
      description,
      topics: topicIds,
      icon: '',
      currentUser: '',
      members
    };
    const handleSearchUser = debounce(async e => {
      const name = e.target.value;
      const {
        data: { userByName }
      } = await searchUser({ variables: { name } });
      setFetchedUsers(userByName);
    }, 600);

    return (
      <DefaultContent padding="40px 0">
        <Fieldset disabled={loading && !error} aria-busy={loading && !error}>
          <Header>
            <Back onClick={() => props.history.push('/teams')} />
            <h1>create team</h1>
          </Header>
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={values => editTeamHandler(values, editTeam)}
          >
            {props =>
              handleChanges(props) || (
                <FormStyle>
                  <Field
                    name="name"
                    render={props => (
                      <TextInput
                        {...props}
                        id="name"
                        label="Team Name"
                        serverError={editTeamResult.error}
                        errorIdentifier="name"
                        displayError={true}
                      />
                    )}
                  />
                  <Field
                    name="icon"
                    render={props => (
                      <FileInput
                        {...props}
                        disabled={disabled}
                        setDisabled={setDisabled}
                        id="icon"
                        label="Team Icon"
                      />
                    )}
                  />
                  <Field
                    name="currentUser"
                    render={props => {
                      const {
                        field: { onChange }
                      } = props;
                      return (
                        <SearchInput
                          fetchedUsers={fetchedUsers}
                          setFetchedUsers={setFetchedUsers}
                          {...props}
                          id="currentUser"
                          label="Add Team Users"
                          gridArea="searchUser"
                          onChange={e => {
                            onChange(e);
                            handleSearchUser(e);
                          }}
                        />
                      );
                    }}
                  />
                  <Field
                    name="members"
                    render={props => (
                      <UsersContainer
                        marginTop="-6px"
                        gridArea="members"
                        {...props}
                      />
                    )}
                  />
                  <Field
                    name="description"
                    render={props => (
                      <Textarea
                        gridArea="description"
                        {...props}
                        height="70px"
                        id="description"
                        label="Team Description"
                        placeholder="Add team description here..."
                      />
                    )}
                  />
                  <Field
                    name="topics"
                    render={props => (
                      <TeamTopics
                        gridArea="topics"
                        {...props}
                        label="team topics"
                        id="topics"
                      />
                    )}
                  />
                  <Button
                    gridArea="submit"
                    auto
                    disabled={disabled || disabledForm || loadingForm}
                    def
                    type="submit"
                  >
                    updat{loadingForm ? 'ing' : 'e'} team
                  </Button>
                </FormStyle>
              )
            }
          </Formik>
        </Fieldset>
      </DefaultContent>
    );
  }
};

const Fieldset = styled.fieldset`
  border: none;
  width: 100%;
  height: fit-content;
  background-color: transparent;
  &:disabled {
    opacity: 0.5;
  }
  &[aria-busy='true'] {
    opacity: 0.5;
  }
`;

const FormStyle = styled(Form)`
  margin-top: 30px;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: 24px;
  grid-row-gap: 5px;
  grid-template-areas:
    'name icon'
    'description .'
    'searchUser .'
    'members members'
    'topics topics'
    'submit submit';
`;

export default compose(
  graphql(getTeamDetails, {
    options: props => {
      return {
        variables: {
          teamId: props.match.params.teamId
        },
        fetchPolicy: 'no-cache'
      };
    },
    name: 'team'
  }),
  graphql(editTeam, { options: { fetchPolicy: 'no-cache' }, name: 'editTeam' }),
  graphql(searchUser, {
    options: { fetchPolicy: 'no-cache' },
    name: 'searchUser'
  })
)(EditTeam);
