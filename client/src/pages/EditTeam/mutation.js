import gql from 'graphql-tag';

const editTeam = gql`
  mutation($teamInput: TeamInput!, $teamId: ID!) {
    editTeam(teamInput: $teamInput, teamId: $teamId) {
      _id
    }
  }
`;

export { editTeam };
