import gql from 'graphql-tag';

const getTeamDetails = gql`
  query($teamId: ID!) {
    team(teamId: $teamId) {
      _id
      name
      icon
      description
      admin
      unfinishedSectionVote
      members {
        _id
        firstName
        lastName
        avatar
        role
      }
      topics {
        _id
        name
        icon
        largeIcon
        positive_criteria
        negative_criteria
        public
      }
    }
  }
`;

const searchUser = gql`
  mutation($name: String) {
    userByName(name: $name) {
      _id
      firstName
      lastName
      fullName
      avatar
      role
    }
  }
`;

export { getTeamDetails, searchUser };
