import gql from 'graphql-tag';

const signin = gql`
  mutation($email: String!, $password: String!) {
    signin(email: $email, password: $password) {
      email
    }
  }
`;

const googleSignIn = gql`
  mutation($googleToken: ID!) {
    userGoogleRegister(googleToken: $googleToken) {
      email
    }
  }
`;

export { signin, googleSignIn };
