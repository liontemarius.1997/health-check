import React, { useState, useEffect } from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import styled from 'styled-components';
import { GoogleLogin } from 'react-google-login';
import { compose, graphql } from 'react-apollo';

import { Register, TextInput, PasswordInput, Button } from '../../components';
import { signin, googleSignIn } from './mutation';
import { me } from '../Routes';

const validationSchema = Yup.object({
  email: Yup.string()
    .email('Invalid email')
    .required('This field is required'),
  password: Yup.string().required('This field is required')
});

const initialValues = {
  email: '',
  password: ''
};

const Login = props => {
  useEffect(() => {
    document.title = 'Login';
  }, []);
  const [loading, setLoading] = useState(false);
  const { signin, signinResult, googleSignIn } = props;
  const signinHandler = async values => {
    setLoading(true);
    signin({ variables: values })
      .then(() => setLoading(false))
      .catch(() => setLoading(false));
  };
  return (
    <Register disabled={loading}>
      <h1>health check login</h1>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={values => signinHandler(values)}
      >
        <LoginStyle>
          <GoogleLogin
            clientId="googleClientId"
            buttonText="Login with Google"
            theme="light"
            isSignedIn={false}
            onFailure={error => console.log(error)}
            onSuccess={e =>
              googleSignIn({
                variables: {
                  googleToken: e.getAuthResponse().id_token
                }
              })
            }
          />

          <DivideSection>
            <span />
            <p>or</p>
            <span />
          </DivideSection>
          <FormGroupsStyle>
            <Field
              name="email"
              render={props => (
                <TextInput
                  {...props}
                  serverError={signinResult.error}
                  displayError={false}
                  label="E-mail"
                  id="email"
                />
              )}
            />
            <Field
              name="password"
              render={props => (
                <PasswordInput
                  {...props}
                  label="Password"
                  id="password"
                  serverError={signinResult.error}
                  errorIdentifier="The username/password combination is not valid."
                />
              )}
            />
          </FormGroupsStyle>

          <Button def type="submit" disabler={loading}>
            login
          </Button>

          <LinkSectionStyle>
            <p onClick={() => props.history.push('/register')}>register page</p>
            <p onClick={() => props.history.push('/forgot-password')}>
              forgot password?
            </p>
          </LinkSectionStyle>
        </LoginStyle>
      </Formik>
    </Register>
  );
};

export default compose(
  graphql(signin, {
    options: { refetchQueries: [{ query: me }] },
    name: 'signin'
  }),
  graphql(googleSignIn, {
    options: { refetchQueries: [{ query: me }] },
    name: 'googleSignIn'
  })
)(Login);

const LoginStyle = styled(Form)`
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 6px;
`;

const FormGroupsStyle = styled.div`
  width: 100%;
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 6px;
  margin-bottom: 10px;
`;

const LinkSectionStyle = styled.div`
  width: 100%;
  height: fit-content;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 12px;
  p {
    text-transform: capitalize;
    color: ${({ theme }) => theme.color.primary};
    font-weight: 900;
    font-size: 12px;
    text-decoration: underline;
    cursor: pointer;
  }
`;

const DivideSection = styled.div`
  padding: 10px 0;
  display: flex;
  justify-content: center;
  align-items: center;
  span {
    height: 1px;
    background-color: #dddddd;
    width: 100%;
  }
  p {
    padding: 0 10px;
    margin: 0;
    font-size: 12px;
    color: #dddddd;
    text-transform: uppercase;
  }
`;
