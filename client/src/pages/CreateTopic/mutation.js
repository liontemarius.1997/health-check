import gql from 'graphql-tag';

const CREATE_TOPIC_MUTATION = gql`
  mutation CREATE_TOPIC_MUTATION($topicInput: TopicInput!) {
    createTopic(topicInput: $topicInput) {
      _id
    }
  }
`;

export { CREATE_TOPIC_MUTATION };
