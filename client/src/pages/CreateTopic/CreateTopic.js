import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { Mutation } from 'react-apollo';

import {
  DefaultContent,
  Header,
  TextInput,
  Textarea,
  Button,
  FileInput,
  Back
} from '../../components';

import { CREATE_TOPIC_MUTATION } from '.';

const validationSchema = Yup.object({
  name: Yup.string().required('Topic name is required'),
  icon: Yup.mixed().required('Topic icon is required'),
  positive_criteria: Yup.string().required('Positive criteria is required'),
  negative_criteria: Yup.string().required('Negative criteria is required')
});

const initialValues = {
  name: '',
  icon: '',
  positive_criteria: '',
  negative_criteria: '',
  largeIcon: ''
};

const CreateTopic = props => {
  useEffect(() => {
    document.title = 'Create Topic';
  }, []);
  const [disabled, setDisabled] = useState(false);
  const createTopicHandler = (values, createTopic) => {
    createTopic({
      variables: {
        topicInput: values
      }
    }).then(() => props.history.push('/topics'));
  };
  return (
    <DefaultContent padding="40px 0">
      <Mutation fetchPolicy="no-cache" mutation={CREATE_TOPIC_MUTATION}>
        {(createTopic, { loading, error }) => (
          <Fieldset disabled={loading && !error} aria-busy={loading && !error}>
            <Header>
              <Back onClick={() => props.history.push('/topics')} />
              <h1>create topic</h1>
            </Header>
            <Formik
              initialValues={initialValues}
              validationSchema={validationSchema}
              onSubmit={values => createTopicHandler(values, createTopic)}
            >
              <FormStyle>
                <Field
                  name="name"
                  render={props => (
                    <TextInput
                      displayError={true}
                      serverError={error}
                      errorIdentifier="This name is already in use. Please use another name in order to create the topic!"
                      {...props}
                      id="name"
                      label="Topic Name"
                    />
                  )}
                />
                <Field
                  name="icon"
                  render={props => (
                    <FileInput
                      {...props}
                      disabled={disabled}
                      setDisabled={setDisabled}
                      id="icon"
                      label="Topic Icon"
                    />
                  )}
                />
                <Field
                  name="positive_criteria"
                  render={props => (
                    <Textarea
                      height="117px"
                      {...props}
                      id="positive_criteria"
                      label="Topic Positive Criteria"
                      placeholder="Add topic negative criteria here..."
                    />
                  )}
                />
                <Field
                  name="negative_criteria"
                  render={props => (
                    <Textarea
                      placeholder="Add topic positive criteria here..."
                      height="117px"
                      {...props}
                      id="negative_criteria"
                      label="Topic Negative Criteria"
                    />
                  )}
                />
                <ButtonWrapper>
                  <Button disabled={disabled || loading} auto def type="submit">
                    creat{loading ? 'ing' : 'e'} topic
                  </Button>
                </ButtonWrapper>
              </FormStyle>
            </Formik>
          </Fieldset>
        )}
      </Mutation>
    </DefaultContent>
  );
};

const Fieldset = styled.fieldset`
  border: none;
  width: 100%;
  height: fit-content;
  background-color: transparent;
  &:disabled {
    opacity: 0.5;
  }
  &[aria-busy='true'] {
    opacity: 0.5;
  }
`;

const FormStyle = styled(Form)`
  margin-top: 30px;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 24px;
  grid-template-areas:
    'name icon'
    'positive negative'
    'button button';
`;

const ButtonWrapper = styled.div`
  grid-area: button;
  justify-self: center;
  align-self: center;
`;

export { CreateTopic };
