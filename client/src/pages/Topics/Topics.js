import React, { useEffect } from 'react';
import { Query } from 'react-apollo';
import { Redirect } from 'react-router-dom';

import { TOPICS_QUERY } from './query';
import {
  TopicCard,
  ExtendedHeader,
  CardsContainer,
  DefaultContent,
  Button,
  Spinner,
  EmptyContent
} from '../../components';

const Topics = props => {
  useEffect(() => {
    document.title = 'Topics';
  }, []);
  return (
    <DefaultContent padding="40px 0">
      <ExtendedHeader>
        <div>
          <h1>topics</h1>
        </div>
        <Button
          auto
          def
          onClick={() => props.history.push('/topics/create-topic')}
        >
          create topic
        </Button>
      </ExtendedHeader>
      <Query query={TOPICS_QUERY} fetchPolicy="no-cache">
        {({ data, loading, error }) => {
          if (loading) return <Spinner />;
          if (error) return <Redirect to="/login" />;
          if (!loading && !error) {
            const { topics } = data;
            if (topics.length === 0)
              return (
                <EmptyContent
                  firstRow="There is no topic to health check at the moment."
                  secondRow="Help Doc wake up!"
                />
              );
            return (
              <CardsContainer marginTop="40px" columnsNum="4">
                {topics.map((topic, index) => (
                  <TopicCard
                    key={index}
                    onClick={() => props.history.push(`/topics/${topic._id}`)}
                    pointer
                    {...topic}
                  />
                ))}
              </CardsContainer>
            );
          }
        }}
      </Query>
    </DefaultContent>
  );
};
export { Topics };
