import gql from 'graphql-tag';

const TOPIC_DETAILS_FRAGMENT = gql`
  fragment TopicDetails on Topic {
    _id
    positive_criteria
    negative_criteria
    icon
    name
  }
`;

const TOPICS_QUERY = gql`
  query TOPICS_QUERY {
    topics {
      ...TopicDetails
    }
  }
  ${TOPIC_DETAILS_FRAGMENT}
`;
export { TOPICS_QUERY, TOPIC_DETAILS_FRAGMENT };
