import gql from 'graphql-tag';

const REQUEST_RESET_MUTATION = gql`
  mutation REQUEST_RESET_MUTATION($email: String!) {
    requestReset(email: $email) {
      msg
    }
  }
`;

export { REQUEST_RESET_MUTATION };
