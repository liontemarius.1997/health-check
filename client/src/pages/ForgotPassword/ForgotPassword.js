import React, { useEffect } from 'react';
import styled from 'styled-components';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { Mutation } from 'react-apollo';

import { REQUEST_RESET_MUTATION } from '.';

import { Register, Button, TextInput } from '../../components';

const initialValues = {
  email: ''
};

const validationSchema = Yup.object({
  email: Yup.string()
    .email('Invalid email')
    .required('This field is required')
});

const ForgotPassword = props => {
  useEffect(() => {
    document.title = 'Reset Password';
  }, []);
  const resetPasswordHandler = async ({ email }, requestReset) => {
    await requestReset({
      variables: {
        email
      }
    });
  };
  return (
    <Mutation mutation={REQUEST_RESET_MUTATION} fetchPolicy="no-cache">
      {(requestReset, { loading, error }) => (
        <Register disabled={loading}>
          <h1>health check reset password</h1>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={values => resetPasswordHandler(values, requestReset)}
          >
            <FormStyle>
              <Field
                name="email"
                render={props => (
                  <TextInput
                    {...props}
                    id="email"
                    label="Your email"
                    serverError={error}
                    errorIdentifier=" "
                    displayError={true}
                  />
                )}
              />
              <Button def type="submit">
                Sent email
              </Button>
            </FormStyle>
          </Formik>
        </Register>
      )}
    </Mutation>
  );
};
export { ForgotPassword };

const FormStyle = styled(Form)`
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 6px;
`;
