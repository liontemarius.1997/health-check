import React, { useEffect } from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import styled from 'styled-components';
import { Mutation } from 'react-apollo';

import RESET_PASSWORD_MUTATION from './mutation';

import { Register, PasswordInput, Button } from '../../components';

const validationSchema = Yup.object({
  password: Yup.string().required('This field is required')
});

const initialValues = {
  password: ''
};

const ResetPassword = props => {
  useEffect(() => {
    document.title = 'Reset Password';
  }, []);
  const { token } = props.match.params;
  const resetPasswordHandler = (values, resetPassword) => {
    const { password } = values;
    resetPassword({
      variables: {
        resetToken: token,
        password
      }
    }).then(() => props.history.push('/login'));
  };
  return (
    <Mutation mutation={RESET_PASSWORD_MUTATION} fetchPolicy="no-cache">
      {(resetPassword, { loading, error }) => (
        <Register disabled={loading}>
          <h1>health check rest password</h1>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={values => resetPasswordHandler(values, resetPassword)}
          >
            <FormStyle>
              <Field
                name="password"
                render={props => (
                  <PasswordInput
                    {...props}
                    label="Type new password"
                    id="password"
                    serverError={error}
                    errorIdentifier=" "
                    displayError={true}
                  />
                )}
              />
              <Button def type="submit">
                reset password
              </Button>
            </FormStyle>
          </Formik>
        </Register>
      )}
    </Mutation>
  );
};
export { ResetPassword };

const FormStyle = styled(Form)`
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 6px;
`;
