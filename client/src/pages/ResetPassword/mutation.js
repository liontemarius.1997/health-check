import gql from 'graphql-tag';

const RESET_PASSWORD_MUTATION = gql`
  mutation RESET_PASSWORD_MUTATION($password: String!, $resetToken: String!) {
    resetPassword(password: $password, resetToken: $resetToken) {
      _id
    }
  }
`;

export default RESET_PASSWORD_MUTATION;
