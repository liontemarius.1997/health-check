import Topic from './Topic/Topic';
import EditTopic from './EditTopic/EditTopic';
import Teams from './Teams/Teams';
import EditTeam from './EditTeam/EditTeam';
import CreateTeam from './CreateTeam/CreateTeam';
import CreateStat from './CreateStat/CreateStat';
import Login from './Login/Login';
import User from './User/User';
import Routes from './Routes/Routes';
export {
  Topic,
  EditTopic,
  Teams,
  EditTeam,
  CreateTeam,
  CreateStat,
  Login,
  User,
  Routes
};
export * from './Home/Home';
export * from './Topics/Topics';
export * from './CreateTopic/CreateTopic';
export * from './Stats/Stats';

export * from './Team/Team';
export * from './Register/Register';
export * from './NotFound';

export * from './ResetPassword/ResetPassword';
export * from './ForgotPassword/ForgotPassword';
