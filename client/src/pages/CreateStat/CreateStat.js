import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Formik, Form } from 'formik';
import { Query, compose, graphql } from 'react-apollo';
import { Redirect } from 'react-router-dom';

import { Button, Spinner, TopicVote } from '../../components';
import {
  TOPIC_QUERY,
  teamDetails,
  createSession,
  autosaveVoteSession
} from '.';

const CreateStat = props => {
  useEffect(() => {
    document.title = 'Health Check';
  }, []);
  const { team, autosaveVoteSession, createSession } = props;
  const [topicIndex, setTopicIndex] = useState(0);
  const [loadingForm, setLoadingForm] = useState(false);
  const { teamId } = props.match.params;

  const { team: currentTeam, loading, error } = team;

  if (loading || error) {
    return (
      <ContainerStyle>
        {loading && <Spinner />}
        {error && <Redirect to="/login" />}
      </ContainerStyle>
    );
  }
  if (!loading && !error) {
    const { topics, members, unfinishedSectionVote } = currentTeam;
    const topicIds = topics.map(topic => topic._id);
    let initialValues = {};
    if (unfinishedSectionVote !== '') {
      initialValues = JSON.parse(unfinishedSectionVote);
    } else {
      topics.forEach(topic => {
        let topicVotesKey = `${topic._id}VOTES`;
        initialValues[topicVotesKey] = '';
        initialValues[topic._id] = '';
      });
    }

    const disableButtonHandler = values => {
      return values[`${topicIds[topicIndex]}VOTES`].length > 0
        ? values[`${topicIds[topicIndex]}VOTES`].length === members.length + 2
          ? false
          : true
        : true;
    };

    const createSessionHandler = values => {
      const sessionInput = topicIds.map(topicId => {
        setLoadingForm(true);
        const trend = parseInt(
          values[`${topicId}VOTES`].find(({ id }) => id === 'trend').value
        );
        const overallRating = parseInt(
          values[`${topicId}VOTES`].find(({ id }) => id === 'overall-rating')
            .value
        );
        let topicActions = [];
        if (values[topicId].includes('*')) {
          const topicActionsArray = values[topicId]
            .split('* ')
            .filter(topicAction => topicAction !== '');
          topicActions = topicActionsArray.map(action => ({
            text: action,
            checked: false
          }));
        }
        return {
          topicId,
          trend,
          overallRating,
          topicActions
        };
      });
      createSession({
        variables: {
          sessionInput: { topicsSession: sessionInput },
          teamId,
          unfinishedSectionVote: ''
        }
      })
        .then(_ => props.history.push(`/teams/${props.match.params.teamId}`))
        .catch(e => console.log(e));
    };

    const cancelHandler = () => {
      autosaveVoteSession({
        variables: {
          teamId,
          sessionContent: ''
        }
      }).then(() => props.history.push(`/teams/${teamId}`));
    };

    return (
      <ContainerStyle>
        <Formik
          initialValues={initialValues}
          onSubmit={values => createSessionHandler(values)}
        >
          {props => (
            <ContentWrapperStyle>
              <Query
                fetchPolicy="no-cache"
                query={TOPIC_QUERY}
                variables={{ topicId: topics[topicIndex]._id }}
              >
                {({ data, loading, error }) => {
                  if (loading) return <Spinner />;
                  if (error) return <p>Error</p>;
                  if (!loading && !error) {
                    return (
                      <TopicVote
                        teamId={teamId}
                        autosaveSession={autosaveVoteSession}
                        topic={data.topic}
                        formProps={props}
                        members={members}
                      />
                    );
                  }
                }}
              </Query>
              <FooterStyle>
                {topicIndex === 0 ? (
                  <Button
                    type="button"
                    sec
                    auto
                    onClick={() => cancelHandler()}
                  >
                    cancel
                  </Button>
                ) : (
                  <Button
                    sec
                    auto
                    type="button"
                    onClick={() => setTopicIndex(topicIndex - 1)}
                  >
                    back
                  </Button>
                )}
                <PaginationContainer>
                  <h1>topics completed</h1>
                  <PageNumber>
                    <p>
                      {topicIndex + 1}/{topics.length}
                    </p>
                    <PagesContainer>
                      {topics.map((_, index) =>
                        index <= topicIndex ? (
                          <PageStyle key={index} active />
                        ) : (
                          <PageStyle key={index} />
                        )
                      )}
                    </PagesContainer>
                  </PageNumber>
                </PaginationContainer>
                {topicIndex + 1 < topics.length && (
                  <Button
                    disabled={disableButtonHandler(props.values)}
                    def
                    auto
                    type="button"
                    onClick={() => setTopicIndex(topicIndex + 1)}
                  >
                    next
                  </Button>
                )}
                {topicIndex + 1 === topics.length && (
                  <Button
                    type="submit"
                    def
                    auto
                    disabled={disableButtonHandler(props.values) || loadingForm}
                  >
                    finish{loadingForm ? 'ing' : ''}
                  </Button>
                )}
              </FooterStyle>
            </ContentWrapperStyle>
          )}
        </Formik>
      </ContainerStyle>
    );
  }
};

export default compose(
  graphql(teamDetails, {
    options: props => {
      return {
        variables: {
          teamId: props.match.params.teamId
        },
        fetchPolicy: 'no-cache'
      };
    },
    name: 'team'
  }),
  graphql(createSession, {
    options: { fetchPolicy: 'no-cache' },
    name: 'createSession'
  }),
  graphql(autosaveVoteSession, {
    options: { fetchPolicy: 'no-cache' },
    name: 'autosaveVoteSession'
  })
)(CreateStat);

const ContainerStyle = styled.div`
  z-index: 100;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  max-height: 100vh;
  position: absolute;
  display: block;
  background-color: #ffff;
`;

const ContentWrapperStyle = styled(Form)`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-rows: 1fr 72px;
`;

const FooterStyle = styled.div`
  display: grid;
  grid-auto-flow: column;
  padding: 0 52px;
  align-items: center;
  justify-content: space-between;
  background-color: #f6f6f6;
`;

const PaginationContainer = styled.div`
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 5px;
  h1 {
    margin: 0;
    text-transform: uppercase;
    font-size: 12px;
    font-weight: 500;
    color: #191919;
  }
`;

const PageNumber = styled.div`
  display: grid;
  grid-auto-flow: column;
  grid-column-gap: 10px;
  p {
    margin: 0;
    font-size: 18px;
    font-weight: 900;
    color: #191919;
  }
`;

const PagesContainer = styled.div`
  display: grid;
  grid-auto-flow: column;
  width: fit-content;
  grid-column-gap: 4px;
`;

const PageStyle = styled.span`
  height: 20px;
  width: 26px;
  background-color: ${({ active }) => (active ? '#55b37e' : '#d8d8d8')};
  &:last-child {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
  }
  &:first-child {
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
  }
`;
