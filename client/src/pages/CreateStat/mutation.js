import gql from 'graphql-tag';

const createSession = gql`
  mutation(
    $sessionInput: SessionInput!
    $teamId: ID!
    $unfinishedSectionVote: String
  ) {
    createSession(
      teamId: $teamId
      sessionInput: $sessionInput
      unfinishedSectionVote: $unfinishedSectionVote
    ) {
      sessions {
        topicsSession {
          topicId
        }
      }
    }
  }
`;

const autosaveVoteSession = gql`
  mutation($teamId: ID!, $sessionContent: String!) {
    autosaveVoteSession(teamId: $teamId, sessionContent: $sessionContent) {
      _id
    }
  }
`;

export { createSession, autosaveVoteSession };
