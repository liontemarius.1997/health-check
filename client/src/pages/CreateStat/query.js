import gql from 'graphql-tag';

import { TOPIC_DETAILS_FRAGMENT } from '../Topics/query';

const TOPIC_QUERY = gql`
  query TOPIC_QUERY($topicId: ID!) {
    topic(topicId: $topicId) {
      ...TopicDetails
      largeIcon
    }
  }
  ${TOPIC_DETAILS_FRAGMENT}
`;

const teamDetails = gql`
  query($teamId: ID!) {
    team(teamId: $teamId) {
      _id
      name
      icon
      description
      editPermission
      admin
      unfinishedSectionVote
      members {
        _id
        firstName
        lastName
        avatar
        role
      }
      topics {
        _id
        name
        icon
        largeIcon
        positive_criteria
        negative_criteria
        public
      }
    }
  }
`;

export { TOPIC_QUERY, teamDetails };
