import gql from 'graphql-tag';

const TEAM_DETAILS_FRAGMENT = gql`
  fragment TeamDetails on Team {
    _id
    description
    name
    icon
  }
`;

const TEAMS_QUERY = gql`
  query TEAMS_QUERY {
    teams {
      ...TeamDetails
      membersNumber
      lastSession {
        createdAt
      }
    }
  }
  ${TEAM_DETAILS_FRAGMENT}
`;

export { TEAMS_QUERY };
