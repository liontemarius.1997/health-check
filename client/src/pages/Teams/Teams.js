import React, { useEffect } from 'react';
import { Query } from 'react-apollo';
import { Redirect } from 'react-router-dom';

import {
  ExtendedHeader,
  CardsContainer,
  DefaultContent,
  Button,
  TeamCard,
  Spinner,
  EmptyContent
} from '../../components';
import { TEAMS_QUERY } from '.';

const Teams = props => {
  useEffect(() => {
    document.title = 'Teams';
  }, []);
  return (
    <DefaultContent padding="40px 0px">
      <ExtendedHeader>
        <div>
          <h1>Teams</h1>
        </div>
        <Button
          auto
          def
          onClick={() => props.history.push('/teams/create-team')}
        >
          create new team
        </Button>
      </ExtendedHeader>

      <Query query={TEAMS_QUERY} fetchPolicy="no-cache">
        {({ data, loading, error }) => {
          if (loading) return <Spinner />;
          if (error) return <Redirect to="/login" />;
          if (!loading && !error) {
            const { teams } = data;
            if (teams.length === 0)
              return (
                <EmptyContent
                  firstRow="There is no team to health check at the moment."
                  secondRow="Help Doc wake up!"
                />
              );
            return (
              <CardsContainer columnsNum="4" marginTop="40px">
                {teams.map(
                  ({
                    _id,
                    name,
                    membersNumber,
                    description,
                    icon,
                    lastSession
                  }) => (
                    <TeamCard
                      key={_id}
                      name={name}
                      membersNumber={membersNumber}
                      description={
                        description.length > 120
                          ? `${description.slice(0, 120)}...`
                          : description
                      }
                      icon={icon}
                      data={lastSession}
                      onClick={() => props.history.push(`/teams/${_id}`)}
                    />
                  )
                )}
              </CardsContainer>
            );
          }
        }}
      </Query>
    </DefaultContent>
  );
};
export default Teams;
