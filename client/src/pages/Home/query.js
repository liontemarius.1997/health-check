import gql from 'graphql-tag';

const SESSIONS_TEAM_QUERY = gql`
  query SESSIONS_TEAM_QUERY($teamId: ID!, $limit: Int) {
    sessions(teamId: $teamId, limit: $limit) {
      _id
      createdAt
      topicsSession {
        trend
        overallRating
        _id
        topicId
        icon
        name
        topicActions {
          text
          checked
          _id
        }
      }
    }
  }
`;

export { SESSIONS_TEAM_QUERY };
