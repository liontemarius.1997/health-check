import React, { useEffect } from 'react';
import { Query } from 'react-apollo';
import { Redirect } from 'react-router-dom';
import styled from 'styled-components';

import { SESSIONS_TEAM_QUERY } from '.';
import {
  Content,
  Header,
  Spinner,
  CriteriaCard,
  Info,
  TopicCard,
  CheckboxAction,
  EmptyContent,
  RedirectLink
} from '../../components';

const Home = props => {
  useEffect(() => {
    document.title = 'Home';
  }, []);
  const topicsSessions = sessions => {
    let topics = [];
    sessions.forEach(({ topicsSession }) => {
      topics = [...topics, ...topicsSession];
    });

    return topics;
  };
  const topicSections = topics => {
    let positive = [];
    let negative = [];
    topics.forEach(topic => {
      const { trend, overallRating, topicActions } = topic;
      const topicActionsChecked = topicActions.filter(({ checked }) => checked)
        .length;
      if (trend >= 1 && overallRating > 1) {
        positive.push(topic);
      } else if (trend <= 1 && overallRating <= 1) {
        if (
          (topicActions.length > 0 &&
            topicActionsChecked < topicActions.length) ||
          topicActions.length === 0
        ) {
          if (overallRating < 1 && trend < 1) {
            negative.unshift(topic);
          } else {
            negative.push(topic);
          }
        }
      }
    });

    const topicsWithActions = topics.filter(
      topic => topic.topicActions.length > 0
    );
    return {
      positive: positive.slice(0, 5),
      negative: negative.slice(0, 5),
      topicsWithActions
    };
  };

  return (
    <Content padding="25px 0">
      {teamId => (
        <React.Fragment>
          <Header>
            <h1>Health Check Summary</h1>
          </Header>
          <Query
            query={SESSIONS_TEAM_QUERY}
            fetchPolicy="no-cache"
            variables={{ teamId, limit: 1 }}
          >
            {({ data, loading, error }) => {
              if (loading) return <Spinner />;
              if (error) return <Redirect to="/login" />;
              if (!loading && !error) {
                const { sessions } = data;
                const topics = topicsSessions(sessions);
                const { positive, negative, topicsWithActions } = topicSections(
                  topics
                );
                const topicColumnContainer = () => {
                  let column1 = [];
                  let column2 = [];
                  let column3 = [];
                  let i = 1;
                  topicsWithActions.forEach(topic => {
                    if (i === 1) {
                      column1.push(topic);
                      i++;
                    } else if (i === 2) {
                      column2.push(topic);
                      i++;
                    } else {
                      column3.push(topic);
                      i = 1;
                    }
                  });
                  return { column1, column2, column3 };
                };
                const { column1, column2, column3 } = topicColumnContainer();
                return (
                  <ContainerStyle>
                    {topics.length > 0 ? (
                      <React.Fragment>
                        {positive.length > 0 && negative.length > 0 && (
                          <StatsCardsContainer>
                            <CriteriaCard
                              type="criteria"
                              positive
                              description="These are topics your team members are best at according to their voting patterns."
                              results={positive}
                            />
                            <CriteriaCard
                              type="criteria"
                              description="These are topics your team members are best at according to their voting patterns."
                              results={negative}
                            />
                          </StatsCardsContainer>
                        )}
                        <Info positive={positive} negative={negative} />
                        {column1.length > 0 && (
                          <ActionItemsContainer>
                            <h1>Action Items</h1>
                            <ActionItemsColumns>
                              <ColumnComponent topicActions={column1} />
                              <ColumnComponent topicActions={column2} />
                              <ColumnComponent topicActions={column3} />
                            </ActionItemsColumns>
                          </ActionItemsContainer>
                        )}
                      </React.Fragment>
                    ) : (
                      <EmptyContent
                        firstRow="There is no health check for this team at the moment."
                        secondRow="Help Doc wake up!"
                      >
                        <RedirectLink
                          def="true"
                          auto="true"
                          to={`/teams/${teamId}`}
                        >
                          create a healt check
                        </RedirectLink>
                      </EmptyContent>
                    )}
                  </ContainerStyle>
                );
              }
            }}
          </Query>
        </React.Fragment>
      )}
    </Content>
  );
};

const ColumnComponent = ({ topicActions }) => (
  <Column>
    {topicActions.map((topic, index) => {
      return (
        <TopicCard info key={index} hover="false" {...topic} topicActions>
          {topic.topicActions.map((action, index) => (
            <CheckboxAction
              readOnly={true}
              hover="false"
              key={index}
              {...action}
            />
          ))}
        </TopicCard>
      );
    })}
  </Column>
);

export { Home };

const ContainerStyle = styled.div`
  padding-top: 25px;
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 20px;
`;

const StatsCardsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 24px;
`;

const ActionItemsContainer = styled.div`
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 15px;
  h1 {
    font-size: 20px;
    line-height: 1.2;
    color: #191919;
    font-weight: 400;
  }
`;

const ActionItemsColumns = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 24px;
`;

const Column = styled.div`
  display: flex;
  flex-direction: column;
  & > * {
    margin-bottom: 24px;
  }
`;
