import gql from 'graphql-tag';

const editTopicMutation = gql`
  mutation($topicInput: TopicInput!, $topicId: ID!) {
    editTopic(topicInput: $topicInput, topicId: $topicId) {
      _id
    }
  }
`;

export { editTopicMutation };
