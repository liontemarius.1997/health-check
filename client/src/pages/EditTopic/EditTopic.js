import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { graphql, compose } from 'react-apollo';
import { Redirect } from 'react-router-dom';

import {
  DefaultContent,
  Header,
  TextInput,
  Textarea,
  Button,
  FileInput,
  Back,
  Spinner
} from '../../components';
import { getTopicDetails, editTopicMutation } from '.';

const validationSchema = Yup.object({
  name: Yup.string().required('Topic name is required'),
  positive_criteria: Yup.string().required('Positive criteria is required'),
  negative_criteria: Yup.string().required('Negative criteria is required')
});

const EditTopic = props => {
  useEffect(() => {
    document.title = 'Edit Topic';
  }, []);
  const {
    match: { params },
    editTopic,
    editTopicResult,
    topic: { error, loading, topic }
  } = props;
  const [disabled, setDisabled] = useState(false);
  const [changes, setChanges] = useState(true);
  const [loadingForm, setLoadingForm] = useState(false);
  const changesHandler = (values, initialValues) => {
    setChanges(JSON.stringify(values) === JSON.stringify(initialValues));
    return null;
  };
  const editTopicHandler = values => {
    setLoadingForm(true);
    const { topicId } = props.match.params;

    if (values.icon === '') {
      delete values.icon;
      delete values.largeIcon;
    }
    editTopic({
      variables: { topicInput: values, topicId }
    })
      .then(() => {
        setLoadingForm(false);
        props.history.push('/topics');
      })
      .catch(() => setLoadingForm(false));
  };

  if (loading || error) {
    return (
      <DefaultContent padding="50px 0">
        {loading && <Spinner />}
        {error && <Redirect to="/login" />}
      </DefaultContent>
    );
  }
  if (!loading && !error) {
    const { name, positive_criteria, negative_criteria } = topic;
    const initialValues = {
      name,
      positive_criteria,
      negative_criteria,
      icon: '',
      largeIcon: ''
    };
    return (
      <DefaultContent padding="50px 0">
        <Fieldset disabled={loadingForm}>
          <Header>
            <Back
              onClick={() => props.history.push(`/topics/${params.topicId}`)}
            />
            <h1>create topic</h1>
          </Header>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={values => editTopicHandler(values)}
            render={({ values, initialValues }) =>
              changesHandler(values, initialValues) || (
                <FormStyle>
                  <Field
                    name="name"
                    render={props => (
                      <TextInput
                        displayError={true}
                        serverError={editTopicResult.error}
                        errorIdentifier="This name is already in use. Please use another name in order to create the topic!"
                        {...props}
                        id="name"
                        label="Topic Name"
                      />
                    )}
                  />
                  <Field
                    name="icon"
                    render={props => (
                      <FileInput
                        {...props}
                        disabled={disabled}
                        setDisabled={setDisabled}
                        id="icon"
                        label="Topic Icon"
                      />
                    )}
                  />
                  <Field
                    name="positive_criteria"
                    render={props => (
                      <Textarea
                        height="117px"
                        {...props}
                        id="positive_criteria"
                        label="Topic Positive Criteria"
                        placeholder="Add topic negative criteria here..."
                      />
                    )}
                  />
                  <Field
                    name="negative_criteria"
                    render={props => (
                      <Textarea
                        placeholder="Add topic positive criteria here..."
                        height="117px"
                        {...props}
                        id="negative_criteria"
                        label="Topic Negative Criteria"
                      />
                    )}
                  />
                  <ButtonWrapper>
                    <Button
                      disabled={disabled || changes || loadingForm}
                      auto
                      def
                      type="submit"
                    >
                      edit{loadingForm ? 'ing' : ''} topic
                    </Button>
                  </ButtonWrapper>
                </FormStyle>
              )
            }
          />
        </Fieldset>
      </DefaultContent>
    );
  }
};

export default compose(
  graphql(getTopicDetails, {
    options: props => {
      return {
        variables: {
          topicId: props.match.params.topicId
        },
        fetchPolicy: 'no-cache'
      };
    },
    name: 'topic'
  }),
  graphql(editTopicMutation, { name: 'editTopic' })
)(EditTopic);

const Fieldset = styled.fieldset`
  border: none;
  width: 100%;
  height: fit-content;
  background-color: transparent;
  &:disabled {
    opacity: 0.5;
  }
  &[aria-busy='true'] {
    opacity: 0.5;
  }
`;

const FormStyle = styled(Form)`
  margin-top: 30px;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 24px;
  grid-template-areas:
    'name icon'
    'positive negative'
    'button button';
`;

const ButtonWrapper = styled.div`
  grid-area: button;
  justify-self: center;
  align-self: center;
`;
