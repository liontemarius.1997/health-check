import gql from 'graphql-tag';

const editUserProfile = gql`
  mutation($userInput: UserInput!) {
    editUser(userInput: $userInput) {
      role
    }
  }
`;

export { editUserProfile };
