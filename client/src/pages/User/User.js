import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { compose, graphql } from 'react-apollo';
import { Redirect } from 'react-router-dom';

import { editUserProfile, me, USER_QUERY } from '.';

import {
  DefaultContent,
  Header,
  TextInput,
  Button,
  FileInput,
  PasswordInput,
  Spinner,
  SelectInput
} from '../../components';

const validationSchema = Yup.object({
  firstName: Yup.string().required('First Name is required'),
  lastName: Yup.string().required('Last Name is required')
});

const User = props => {
  useEffect(() => {
    document.title = 'User Page';
  }, []);
  const { me, editUserProfile, editUserProfileResult } = props;
  const { loading, me: currentUser, error } = me;
  const [loadingForm, setLoadingForm] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [changes, setChanges] = useState(false);
  const changesHandler = ({ values, initialValues }) => {
    setChanges(JSON.stringify(values) === JSON.stringify(initialValues));
    return null;
  };
  const resetPasswordHandler = ({ values }) => {
    if (values.password !== '' && values.oldPassword === '') {
      return true;
    } else if (values.password === '' && values.oldPassword !== '') {
      return true;
    } else {
      return false;
    }
  };

  const editUserHandler = values => {
    setLoadingForm(true);
    const currentValues = Object.assign({}, values);
    delete currentValues.largeIcon;
    if (currentValues.avatar === '') {
      delete currentValues.avatar;
    }
    if (currentValues.password === '' || currentValues.newPassword === '') {
      delete currentValues.password;
      delete currentValues.newPassword;
    }
    editUserProfile({ variables: { userInput: currentValues } })
      .then(() => {
        setLoadingForm(false);
        props.history.push('/');
      })
      .catch(() => setLoadingForm(false));
  };
  if (loading || error) {
    return (
      <DefaultContent padding="50px 0">
        {loading && <Spinner />}
        {error && <Redirect to="/login" />}
      </DefaultContent>
    );
  } else {
    const { firstName, lastName, role, googleAccount } = currentUser;
    const initialValues = {
      firstName,
      lastName,
      avatar: '',
      password: '',
      oldPassword: '',
      role
    };
    return (
      <DefaultContent padding="50px 0">
        <Fieldset disabled={loadingForm} aria-busy={loadingForm}>
          <Header>
            <h1>user profile</h1>
          </Header>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={values => {
              editUserHandler(values);
            }}
            render={props =>
              changesHandler(props) || (
                <FormStyle>
                  <Field
                    name="firstName"
                    render={props => (
                      <TextInput label="First Name" id="firstName" {...props} />
                    )}
                  />
                  <Field
                    name="avatar"
                    render={props => (
                      <FileInput
                        disabled={disabled}
                        setDisabled={setDisabled}
                        label="Your Picture"
                        id="avatar"
                        {...props}
                      />
                    )}
                  />
                  <Field
                    name="lastName"
                    render={props => (
                      <TextInput label="First Name" id="lastName" {...props} />
                    )}
                  />
                  {googleAccount || (
                    <Field
                      name="oldPassword"
                      render={props => (
                        <PasswordInput
                          displayError={true}
                          serverError={editUserProfileResult.error}
                          errorIdentifier="Please type in your old password in order to save the changes."
                          id="oldPassword"
                          label="Reset Password"
                          {...props}
                        />
                      )}
                    />
                  )}
                  <Field
                    name="role"
                    render={props => <SelectInput {...props} />}
                  />
                  {googleAccount || (
                    <Field
                      name="password"
                      render={props => (
                        <PasswordInput
                          {...props}
                          label="New Password"
                          id="password"
                        />
                      )}
                    />
                  )}
                  <Button
                    disabled={
                      disabled ||
                      changes ||
                      resetPasswordHandler(props) ||
                      loadingForm
                    }
                    gridArea="submit"
                    type="submit"
                    auto
                    def
                  >
                    save changes
                  </Button>
                </FormStyle>
              )
            }
          />
        </Fieldset>
      </DefaultContent>
    );
  }
};
export default compose(
  graphql(me, { options: { fetchPolicy: 'no-cache' }, name: 'me' }),
  graphql(editUserProfile, {
    options: {
      refetchQueries: [{ query: USER_QUERY }],
      fetchPolicy: 'no-cache'
    },
    name: 'editUserProfile'
  })
)(User);

const Fieldset = styled.fieldset`
  border: none;
  width: 100%;
  height: fit-content;
  background-color: transparent;
  &:disabled {
    opacity: 0.5;
  }
  &[aria-busy='true'] {
    opacity: 0.5;
  }
`;

const FormStyle = styled(Form)`
  padding-top: 25px;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 18px;
  grid-template-areas:
    'first icon'
    'last old'
    'select new'
    'submit submit';
`;
