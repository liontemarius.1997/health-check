import React from 'react';
import { ApolloProvider } from 'react-apollo';

import { client } from './utils';
import { Routes } from './pages';
import { Page } from './components';

function App() {
  return (
    <ApolloProvider client={client}>
      <Page>
        <Routes />
      </Page>
    </ApolloProvider>
  );
}

export default App;
