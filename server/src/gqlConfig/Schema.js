const { typeDefs: team } = require('../types/team');
const { typeDefs: topic } = require('../types/topic');
const { typeDefs: user } = require('../types/user');

module.exports = [topic, team, user].join('');
