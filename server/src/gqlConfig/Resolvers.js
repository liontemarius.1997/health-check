const { resolvers: team } = require('../types/team');
const { resolvers: topic } = require('../types/topic');
const { resolvers: user } = require('../types/user');
const merge = require('lodash/merge');

module.exports = merge({}, team, topic, user);
