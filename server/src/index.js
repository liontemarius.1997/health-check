require('dotenv').config({ path: 'variables.env' });
const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');

const createServer = require('./server');
const User = require('./types/user/user.model');

require('./db')();
const server = createServer();
// Use express middlewares to handle cookies (JWT)
server.express.use(cookieParser());

server.express.use(bodyParser.urlencoded({ extended: true }));

// Decode the JWT so we can get the Id on each request
server.express.use((req, res, next) => {
  const { token } = req.cookies;
  if (!token) {
    return next();
  }
  const { userId } = jwt.verify(token, process.env.APP_SECRET);
  // put the userId onto the request for future request to access it
  req.userId = userId;
  next();
});

// Create a middleware that populates the user oneach request
server.express.use(async (req, res, next) => {
  // if they are not logged in skip this
  if (!req.userId) {
    return next();
  }
  const user = await User.findById(req.userId)
    .lean()
    .exec();
  req.user = user;
  next();
});

server.start(
  {
    cors: {
      credentials: true,
      origin: process.env.FRONTEND_URL
    }
  },
  serv => {
    console.log(`Serve is now running on port http://localhost:${serv.port}`);
  }
);
