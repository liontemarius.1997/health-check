const { GraphQLServer } = require('graphql-yoga');

const Schema = require('./gqlConfig/Schema');
const Resolvers = require('./gqlConfig/Resolvers');
const { asyncMap, findTopicDetails } = require('../utils/functions');

// Create the GraphQL Yoga Server

function server() {
  return new GraphQLServer({
    typeDefs: Schema,
    resolvers: {
      ...Resolvers,
      Session: {
        async topicsSession(obj) {
          const topicIds = obj.topicsSession.map(({ topicId }) => topicId);
          const results = await asyncMap(topicIds, findTopicDetails);
          return obj.topicsSession.map((topic, index) => ({
            ...topic,
            ...results[index]
          }));
        }
      }
    },
    resolverValidationOptions: {
      requireResolversForResolveType: false
    },
    context: req => req
  });
}

module.exports = server;
