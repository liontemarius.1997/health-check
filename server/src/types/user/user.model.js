const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
    trim: true
  },
  lastName: {
    type: String,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true
  },
  avatar: {
    type: String,
    required: true
  },
  role: {
    type: String,
    required: true,
    default: 'Developer'
  },
  googleAccount: {
    type: Boolean,
    default: false
  },
  resetToken: {
    type: String
  },
  resetTokenExpireDate: {
    type: Date
  }
});

userSchema.methods.passwordHashed = async function(password) {
  const salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(password, salt);
};

userSchema.methods.comparePassword = async function() {
  return await bcrypt.compare(password, this.password);
};

module.exports = user = mongoose.model('user', userSchema);
