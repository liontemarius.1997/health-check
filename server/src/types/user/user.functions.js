const User = require('./user.model');
const Team = require('../team/team.model');

const authorization = next => async (parent, args, ctx, info) => {
  if (ctx.request.userId === undefined)
    throw new Error('You shoult be authenticated first');

  const userExist = await User.findById(ctx.request.userId)
    .lean()
    .exec();

  if (Boolean(Object.entries(userExist).length)) {
    return next(parent, args, ctx, info);
  } else {
    throw Error('You shoult be authenticated first');
  }
};

const permission = next => async (parent, args, ctx, info) => {
  const userPermission = await Team.findOne({
    admin: ctx.request.userId
  })
    .lean()
    .exec();

  const permission = userPermission || [];

  if (Boolean(Object.entries(permission).length)) {
    return next(parent, args, ctx, info);
  } else {
    throw new Error('You should be the creator of this team to edit it');
  }
};

module.exports = { authorization, permission };
