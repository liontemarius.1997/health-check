module.exports = {
  typeDefs: require('../../../utils/gqlLoader')('types/user/typeDefs.graphql'),
  resolvers: require('./user.resolvers')
};
