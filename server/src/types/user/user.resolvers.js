//-------MODULES------
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const jwtDecode = require('jwt-decode');
const { promisify } = require('util');
const { randomBytes } = require('crypto');

//-------MODELS------
const User = require('./user.model');

const { makeANiceEmail, transport } = require('../../mail');

const Query = {
  async me(
    _,
    __,
    {
      request: { userId }
    }
  ) {
    if (!userId) {
      return null;
    }
    const user = await User.findById(userId);
    return user;
  }
};

const Mutation = {
  async userByName(_, { name }) {
    if (name === '') return [];
    const pipeline = [
      {
        $addFields: {
          fullName: { $concat: ['$firstName', ' ', '$lastName'] }
        }
      },
      {
        $match: { fullName: { $regex: `${name}`, $options: 'i' } }
      }
    ];
    return await User.aggregate(pipeline).exec();
  },
  async editUser(
    _,
    { userInput },
    {
      request: { userId }
    }
  ) {
    if (user.googleAccount) return;
    const { oldPassword, password } = userInput;
    if (oldPassword !== undefined && password !== undefined) {
      const user = await User.findById(userId)
        .lean()
        .exec();
      const valid = await bcrypt.compare(oldPassword, user.password);
      if (!valid) {
        throw new Error(
          'Please type in your old password in order to save the changes.'
        );
      }
      const salt = await bcrypt.genSalt(10);
      const hashedPassword = await bcrypt.hash(password, salt);
      userInput.password = hashedPassword;
    }
    if (userInput.avatar === '') delete userInput.avatar;
    return await User.findByIdAndUpdate(userId, userInput)
      .lean()
      .exec();
  },
  signout(_, __, ctx) {
    ctx.response.clearCookie('token');
    return { msg: 'Bye bye bitch!' };
  },
  async signup(_, args, ctx) {
    const { email, password } = args.userInput;
    const userEmailUnique = await User.findOne({
      email
    })
      .lean()
      .exec();

    if (userEmailUnique) {
      throw new Error('This email is already in use');
    }
    const user = new User({
      ...args.userInput,
      resetToken: ''
    });
    user.password = await user.passwordHashed(password);
    const newUser = await user.save();
    // Create the JWT token for them
    const token = jwt.sign({ userId: newUser._id }, process.env.APP_SECRET);
    ctx.response.cookie('token', token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24
    });
    return newUser;
  },
  async signin(_, { email, password }, ctx) {
    // check if there is a user with taht email
    const user = await User.findOne({ email });
    if (!user) {
      throw new Error(`The username/password combination is not valid.`);
    }
    // check if their password is correct
    const valid = await bcrypt.compare(password, user.password);
    if (!valid) {
      throw new Error('The username/password combination is not valid.');
    }
    // generate JWT token
    const token = jwt.sign({ userId: user._id }, process.env.APP_SECRET);
    // set cookie with the token
    ctx.response.cookie('token', token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24
    });
    return user;
  },
  async userGoogleRegister(_, args, ctx) {
    let user;
    const { googleToken } = args;
    const tokenContent = await jwtDecode(googleToken);
    const { email, picture, name } = tokenContent;
    const userExist = await User.findOne({ email })
      .lean()
      .exec();
    if (userExist !== null) {
      const token = jwt.sign({ userId: userExist._id }, process.env.APP_SECRET);
      ctx.response.cookie('token', token, {
        httpOnly: true,
        maxAge: 1000 * 60 * 60 * 24
      });
      user = userExist;
    } else {
      const fullName = name;
      const [firstName, ...lastName] = name.split(' ');
      const password = (await promisify(randomBytes)(20)).toString('hex');
      const newUser = new User({
        fullName,
        firstName,
        lastName: lastName.join(' '),
        password,
        avatar: picture,
        email,
        googleAccount: true,
        resetToken: ''
      });
      const token = jwt.sign({ userId: newUser._id }, process.env.APP_SECRET);
      ctx.response.cookie('token', token, {
        httpOnly: true,
        maxAge: 1000 * 60 * 60 * 24
      });
      user = await newUser.save();
    }
    return user;
  },

  async requestReset(_, { email }) {
    const user = await User.findOne({ email })
      .lean()
      .exec();
    if (!user) {
      throw new Error(`No user with this email`);
    }
    const resetToken = (await promisify(randomBytes)(20)).toString('hex');

    await User.findByIdAndUpdate(user._id, {
      resetToken
    });
    const mailRes = await transport.sendMail({
      from: 'liontemarius.1997@gmail.com',
      to: user.email,
      subject: 'Your password reset token',
      html: makeANiceEmail(
        `Your password reset token is here ! \n\n <a href="${
          process.env.FRONTEND_URL
        }/reset-token/${resetToken}">Click here to reset</a>`
      )
    });
    if (!mailRes) throw new Error('Sorry, try again');
    return { msg: 'Success' };
  },

  async resetPassword(_, { password, resetToken }) {
    const user = await User.findOne({
      googleAccount: false,
      resetToken
    })
      .lean()
      .exec();
    if (!user) {
      throw new Error(
        'This token is either invalid or you use a google account'
      );
    }
    const newPassword = await bcrypt.hash(password, 10);
    const updatedUser = await User.findByIdAndUpdate(
      user._id,
      { password: newPassword, resetToken: '' },
      { new: true }
    );
    return updatedUser;
  }
};

module.exports = { Query, Mutation };
