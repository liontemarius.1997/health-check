module.exports = {
  typeDefs: require('../../../utils/gqlLoader')('types/topic/typeDefs.graphql'),
  resolvers: require('./topic.resolvers')
};
