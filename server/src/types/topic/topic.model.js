const mongoose = require('mongoose');

const topicSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  icon: {
    type: String
  },
  largeIcon: {
    type: String
  },
  positive_criteria: {
    type: String,
    required: true,
    trim: true
  },
  negative_criteria: {
    type: String,
    required: true,
    trim: true
  },
  public: {
    type: Boolean,
    default: false
  },
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  }
});

module.exports = topic = mongoose.model('topic', topicSchema);
