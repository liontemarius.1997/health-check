const Topic = require('./topic.model');
const mongoose = require('mongoose');

const ObjectId = mongoose.Types.ObjectId;

const topicSameName = async (userId, { name }, topicId) => {
  const pipeline = [
    {
      $match: {
        $and: [
          {
            $or: [{ owner: ObjectId(userId) }, { public: true }]
          },
          {
            name
          },
          {
            _id: { $not: { $eq: ObjectId(topicId) } }
          }
        ]
      }
    },
    { $limit: 1 }
  ];

  const result = await Topic.aggregate(pipeline).exec();

  if (result.length > 0) {
    throw new Error(
      'This name is already in use. Please use another name in order to create the topic!'
    );
  }
};

const topics = async (_, __, { request: { userId } }) => {
  return Topic.find({
    $or: [{ public: true }, { owner: userId }]
  })
    .lean()
    .exec();
};

const topic = async (_, { topicId }) => {
  try {
    return await Topic.findById(topicId)
      .lean()
      .exec();
  } catch (error) {
    throw new Error(
      'There is no topic for this id. Please try again with another route.'
    );
  }
};

const createTopic = async (_, { topicInput }, { request: { userId } }) => {
  topicInput.owner = userId;

  topicInput.name = topicInput.name.toLowerCase();

  await topicSameName(userId, topicInput);

  return Topic.create(topicInput);
};

const editTopic = async (
  _,
  { topicId, topicInput },
  { request: { userId } }
) => {
  topicInput.name = topicInput.name.toLowerCase();

  await topicSameName(userId, topicInput, topicId);

  const currentTopicPublic = await Topic.findOne({ _id: topicId, public: true })
    .lean()
    .exec();

  if (currentTopicPublic) return;

  return Topic.findByIdAndUpdate(topicId, topicInput, { new: true });
};

module.exports = { topic, topics, createTopic, editTopic };
