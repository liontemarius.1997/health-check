const { authorization } = require('../user/user.functions');

const { topic, topics, editTopic, createTopic } = require('./topic.functions');

const Query = {
  topics: authorization(topics),
  topic: authorization(topic)
};

const Mutation = {
  createTopic: authorization(createTopic),
  editTopic: authorization(editTopic)
};

module.exports = { Query, Mutation };
