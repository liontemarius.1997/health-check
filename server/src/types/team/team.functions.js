const Team = require('./team.model');
const mongoose = require('mongoose');

const ObjectId = mongoose.Types.ObjectId;

const nameInUse = async ({ name }, userId, teamId) => {
  let pipeline = [];

  if (teamId !== undefined) {
    pipeline.push({
      $match: {
        _id: { $not: { $eq: ObjectId(teamId) } }
      }
    });
  }
  pipeline.push(
    {
      $addFields: {
        nameToLowerCase: { $toLower: '$name' }
      }
    },
    {
      $match: {
        $and: [
          {
            nameToLowerCase: name.toLowerCase()
          },
          {
            admin: ObjectId(userId)
          }
        ]
      }
    },
    { $limit: 1 }
  );

  const result = await Team.aggregate(pipeline).exec();

  if (result.length > 0) {
    throw new Error('This name is already in use!');
  }
};

//----------------QUERIES-------------
const team = async (_, args, ctx) => {
  const { teamId } = args;

  try {
    return await Team.findById(teamId)
      .populate('topics members')
      .lean()
      .exec();
  } catch (error) {
    throw new Error(
      'There is no team for this id. Please try again with another route.'
    );
  }
};

const teams = async (_, __, { request: { userId } }) => {
  const teams = await Team.aggregate([
    {
      $addFields: {
        isMember: {
          $cond: [
            {
              $setIsSubset: [[ObjectId(userId)], '$members']
            },
            true,
            false
          ]
        },
        membersNumber: {
          $size: '$members'
        }
      }
    },
    {
      $match: {
        $or: [{ isMember: true }, { admin: ObjectId(userId) }]
      }
    },
    {
      $project: {
        lastSession: { $slice: ['$sessions', -1] },
        _id: '$_id',
        description: '$description',
        icon: '$icon',
        name: '$name',
        members: '$members',
        membersNumber: '$membersNumber'
      }
    }
  ]).exec();

  return teams;
};

const sessions = async (_, { teamId, limit }) => {
  const pipeline = [
    {
      $match: {
        _id: ObjectId(teamId)
      }
    },
    {
      $unwind: '$sessions'
    },
    {
      $project: {
        _id: '$sessions._id',
        createdAt: '$sessions.createdAt',
        topicsSession: '$sessions.topicsSession'
      }
    },
    { $sort: { createdAt: -1 } }
  ];

  if (limit) {
    pipeline.push({ $limit: limit });
  }

  return await Team.aggregate(pipeline).exec();
};

//----------------MUTATIONS----------
const createTeam = async (_, { teamInput }, { request: { userId: admin } }) => {
  await nameInUse(teamInput, admin);

  const adminAdded = teamInput.members.find(id => id === admin);

  if (!adminAdded) {
    teamInput.members.push(admin);
  }

  return Team.create({ ...teamInput, admin });
};

const editTeam = async (_, { teamId, teamInput }, { request: { userId } }) => {
  await nameInUse(teamInput, userId, teamId);

  return Team.findByIdAndUpdate(teamId, teamInput, { new: true });
};

const createSession = async (_, { sessionInput, teamId }) => {
  sessionInput.createdAt = new Date().toISOString();

  return Team.findByIdAndUpdate(
    teamId,
    {
      unfinishedSectionVote: '',
      $push: {
        sessions: sessionInput
      }
    },
    { new: true }
  )
    .lean()
    .exec().sessions;
};

const autosaveVoteSession = async (_, { teamId, sessionContent }) => {
  return Team.findByIdAndUpdate(
    teamId,
    { unfinishedSectionVote: sessionContent },
    { new: true }
  );
};

const updateSession = async (_, args) => {
  const { updatedSession, teamId, currentSessionIndex } = args;

  const currentTeam = await Team.findById(teamId)
    .lean()
    .exec();

  const currentSession = currentTeam.sessions.find(
    session => session._id.toString() === currentSessionIndex
  );

  currentSession.topicsSession = updatedSession;

  return Team.findByIdAndUpdate(teamId, currentTeam, { new: true });
};

module.exports = {
  teams,
  editTeam,
  sessions,
  team,
  createTeam,
  createSession,
  updateSession,
  autosaveVoteSession
};
