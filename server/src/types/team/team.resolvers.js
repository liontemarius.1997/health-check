const { authorization, permission } = require('../user/user.functions');
const {
  teams,
  editTeam,
  sessions,
  team,
  createTeam,
  createSession,
  autosaveVoteSession,
  updateSession
} = require('./team.functions');

const Mutation = {
  createTeam: authorization(createTeam),
  editTeam: authorization(editTeam),
  createSession: authorization(createSession),
  autosaveVoteSession: authorization(autosaveVoteSession),
  updateSession: authorization(updateSession)
};

const Query = {
  team: authorization(team),
  teams: authorization(teams),
  sessions: authorization(sessions)
};

module.exports = { Query, Mutation };
