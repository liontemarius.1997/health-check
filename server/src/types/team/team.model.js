const mongoose = require('mongoose');

const teamSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  icon: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true,
    trim: true
  },
  unfinishedSectionVote: {
    type: String,
    default: ''
  },
  topics: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'topic'
    }
  ],
  admin: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  permissions: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
    }
  ],
  members: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
    }
  ],
  sessions: [
    {
      topicsSession: [
        {
          topicId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'topic'
          },
          topicActions: [
            {
              text: {
                type: String
              },
              checked: {
                type: Boolean
              }
            }
          ],
          trend: {
            type: Number,
            required: true
          },
          overallRating: {
            type: Number,
            required: true
          }
        }
      ],
      createdAt: { type: String }
    }
  ]
});

module.exports = team = mongoose.model('team', teamSchema);
