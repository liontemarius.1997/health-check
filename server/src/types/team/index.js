module.exports = {
  typeDefs: require('../../../utils/gqlLoader')('types/team/typeDefs.graphql'),
  resolvers: require('./team.resolvers')
};
