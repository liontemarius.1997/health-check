const Topic = require('../src/types/topic/topic.model');

const asyncMap = async (array, callback) => {
  let results = [];
  for (let index = 0; index < array.length; index++) {
    const res = await callback(array[index]);
    results.push(res);
  }
  return results;
};

const findTopicDetails = async topicId => {
  return Topic.findById(topicId, { _id: 0 })
    .lean()
    .exec();
};

module.exports = { asyncMap, findTopicDetails };
